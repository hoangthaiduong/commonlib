/*
High-speed in-memory bit stream I/O that supports reading and writing between
0 and 64 bits at a time.  The implementation, which relies heavily on bit
shifts, has been carefully written to ensure that all shifts are between
zero and one less the width of the type being shifted to avoid undefined
behavior.  This occasionally causes somewhat convoluted code.

The following assumptions and restrictions apply:

1. The user must allocate a memory buffer large enough to hold the bit stream,
   whether for reading, writing, or both.  This buffer is associated with the
   bit stream via stream_open(buffer, bytes), which allocates and returns a
   pointer to an opaque bit stream struct.  Call stream_close(stream) to
   deallocate this struct.

2. The stream is either in a read or write state (or, initially, in both
   states).  When done writing, call stream_flush(stream) before entering
   read mode to ensure any buffered bits are output.  To enter read mode,
   call stream_rewind(stream) or stream_rseek(stream, offset) to position
   the stream at the beginning or at a particular bit offset.  Conversely,
   stream_rewind(stream) or stream_wseek(stream, offset) positions the
   stream for writing.  In read mode, the following functions may be called:

     size_t stream_size(stream);
     size_t stream_rtell(stream);
     void stream_rewind(stream);
     void stream_rseek(stream, offset);
     void stream_skip(stream, uint n);
     void stream_align(stream);
     uint stream_read_bit(stream);
     uint64 stream_read_bits(stream, n);

   Each of these read calls has a corresponding write call:

     size_t stream_size(stream);
     size_t stream_wtell(stream);
     void stream_rewind(stream);
     void stream_wseek(stream, offset);
     void stream_pad(stream, n);
     void stream_flush(stream);
     uint stream_write_bit(stream, bit);
     uint64 stream_write_bits(stream, value, n);

3. The stream buffer is an unsigned integer of a user-specified type given
   by the BIT_STREAM_WORD_TYPE macro.  Bits are read and written in units of
   this integer word type.  Supported types are 8, 16, 32, or 64 bits wide.
   The bit width of the buffer is denoted by 'wsize' and can be accessed via
   the global variable stream_word_bits.  A small wsize allows for fine
   granularity reads and writes, and may be preferable when working with many
   small blocks of data that require non-sequential access.  The default
   maximum size of 64 bits ensures maximum speed.  Note that even when
   wsize < 64, it is still possible to read and write up to 64 bits at a time
   using stream_read_bits() and stream_write_bits().

4. If BIT_STREAM_STRIDED is defined, words read from or written to the stream
   may be accessed noncontiguously by setting a power-of-two block size (which
   by default is one word) and a block stride (defaults to zero blocks).  The
   word pointer is always incremented by one word each time a word is accessed.
   Once advanced past a block boundary, the word pointer is also advanced by
   the stride to the next block.  This feature may be used to store blocks of
   data interleaved, e.g. for progressive coding or for noncontiguous parallel
   access to the bit stream  Note that the block size is measured in words,
   while the stride is measured in multiples of the block size.  Strided access
   can have a significant performance penalty.

5. Multiple bits are read and written in order of least to most significant
   bit.  Thus, the statement

       value = stream_write_bits(stream, value, n);

   is essentially equivalent to (but faster than)

       for (i = 0; i < n; i++, value >>= 1)
         stream_write_bit(value & 1);

   when 0 <= n <= 64.  The same holds for read calls, and thus

       value = stream_read_bits(stream, n);

   is essentially equivalent to

       for (i = 0, value = 0; i < n; i++)
         value += (uint64)stream_read_bit() << i;

   Note that it is possible to write fewer bits than the argument 'value'
   holds (possibly even zero bits), in which case any unwritten bits are
   returned.

6. Although the stream_wseek(stream, offset) call allows positioning the
   stream for writing at any bit offset without any data loss (i.e. all
   previously written bits preceding the offset remain valid), for efficiency
   the stream_flush(stream) operation will zero all bits up to the next
   multiple of wsize bits, thus overwriting bits that were previously stored
   at that location.  Consequently, random write access is effectively
   supported only at wsize granularity.  For sequential access, the largest
   possible wsize is preferred due to higher speed.

7. It is up to the user to adhere to these rules.  For performance reasons,
   no error checking is done, and in particular buffer overruns are not
   caught.
*/

#pragma once

#include "macros.h"
#include "template.h"
#include <climits>
#include <cstdint>

/* Bit stream structure (opaque to caller) */
template <typename T = uint64_t, typename = enable_if_unsigned<T>>
class BitStream {
private:
  static constexpr uint wsize = CUINT(BIT_SIZE(T));

  uint bits_ = 0;   /* number of buffered bits (0 <= bits < wsize) */
  T buffer_ = 0; /* buffer for incoming/outgoing bits (buffer < 2^bits) */
  T* ptr_ = nullptr;   /* pointer to next word to be read/written */
  T* begin_ = nullptr; /* beginning of stream */
  T* end_ = nullptr;   /* end of stream (currently unused) */
  bool internal_buf_ = false; /* whether the bit stream uses an internal buffer */

  /* read a single word from memory */
  FORCE_INLINE T
  read_word()
  {
    T w = *ptr_++;
    return w;
  }

  /* write a single word to memory */
  FORCE_INLINE void
  write_word(T value)
  {
    *ptr_++ = value;
  }

public:
  BitStream()
    : begin_(nullptr)
    , end_(nullptr)
    , internal_buf_(false)
  {}

  /* allocate and initialize bit stream to user-allocated buffer */
  BitStream(uint8_t* buffer, size_t bytes)
    : begin_(RCAST<T*>(buffer))
    , end_(begin_ + bytes/sizeof(T))
    , internal_buf_(false)
  {
    rewind();
  }

  /* allocate and initialize bit stream using an internally allocated buffer */
  BitStream(size_t bytes)
    : begin_(RCAST<T*>(new uint8_t[bytes]))
    , end_(begin_ + bytes/sizeof(T))
    , internal_buf_(true)
  {
    rewind();
  }

  ~BitStream()
  {
    if (internal_buf_) {
      delete [] begin_;
    }
  }

  BitStream(const BitStream& other) = delete;
  BitStream& operator=(const BitStream& other) = delete;

  // TODO: what if this function is called on already initialized bit stream?
  void
  init(size_t bytes)
  {
    begin_ = RCAST<T*>(new uint8_t[bytes]);
    end_ = begin_ + bytes/sizeof(T);
    internal_buf_ = true;
    rewind();
  }

  /* pointer to beginning of stream */
  FORCE_INLINE const void*
  data() const
  {
    return begin_;
  }

  /* current byte size of stream (if flushed) */
  FORCE_INLINE size_t
  size() const
  {
    return sizeof(T) * (ptr_-begin_);
  }

  /* byte capacity of stream */
  FORCE_INLINE size_t
  capacity() const
  {
    return sizeof(T) * (end_-begin_);
  }

  /* read single bit (0 or 1) */
  FORCE_INLINE uint
  read_bit()
  {
    if (!bits_) {
      buffer_ = read_word();
      bits_ = wsize;
    }
    bits_--;
    uint bit = CUINT(buffer_) & 1u;
    buffer_ >>= 1;
    return bit;
  }

  /* write single bit (must be 0 or 1) */
  FORCE_INLINE uint
  write_bit(uint bit)
  {
    buffer_ += SCAST<T>(bit) << bits_;
    if (++bits_ == wsize) {
      write_word(buffer_);
      buffer_ = 0;
      bits_ = 0;
    }
    return bit;
  }

  /* read 0 <= n <= 64 bits */
  FORCE_INLINE uint64_t
  read_bits(uint n)
  {
    uint64_t value = buffer_;
    if (bits_ < n) {
      /* keep fetching wsize bits until enough bits are buffered */
      do {
        /* assert: 0 <= bits < n <= 64 */
        buffer_ = read_word();
        value += (uint64_t)buffer_ << bits_;
        bits_ += wsize;
      } while (sizeof(buffer_)<sizeof(value) && bits_<n);
      /* assert: 1 <= n <= bits < n + wsize */
      bits_ -= n;
      if (!bits_) {
        /* value holds exactly n bits; no need for masking */
        buffer_ = 0;
      }
      else {
        /* assert: 1 <= bits < wsize */
        buffer_ >>= wsize-bits_;
        /* assert: 1 <= n <= 64 */
        value &= ((uint64_t)2<<(n-1)) - 1;
      }
    }
    else {
      /* assert: 0 <= n <= bits < wsize <= 64 */
      bits_ -= n;
      buffer_ >>= n;
      value &= ((uint64_t)1<<n) - 1;
    }
    return value;
  }

  /* write 0 <= n <= 64 low bits of value and return remaining bits */
  FORCE_INLINE uint64_t
  write_bits(uint64_t value, uint n)
  {
    /* append bit string to buffer */
    buffer_ += value << bits_;
    bits_ += n;
    /* is buffer full? */
    if (bits_ >= wsize) {
      /* 1 <= n <= 64; decrement n to ensure valid right shifts below */
      value >>= 1;
      n--;
      /* assert: 0 <= n < 64; wsize <= bits <= wsize + n */
      do {
        /* output wsize bits while buffer is full */
        bits_ -= wsize;
        /* assert: 0 <= bits <= n */
        write_word(buffer_);
        /* assert: 0 <= n - bits < 64 */
        buffer_ = value >> (n-bits_);
      } while (sizeof(buffer_)<sizeof(value) && bits_>=wsize);
    }
    /* assert: 0 <= bits < wsize */
    buffer_ &= ((T)1<<bits_) - 1;
    /* assert: 0 <= n < 64 */
    return value >> n;
  }

  /* return bit offset to next bit to be read */
  FORCE_INLINE size_t
  rtell() const
  {
    return wsize*(ptr_-begin_) - bits_;
  }

  /* return bit offset to next bit to be written */
  FORCE_INLINE size_t
  wtell() const
  {
    return wsize*(ptr_-begin_) + bits_;
  }

  /* position stream for reading or writing at beginning */
  FORCE_INLINE void
  rewind()
  {
    ptr_ = begin_;
    buffer_ = 0;
    bits_ = 0;
  }

  /* position stream for reading at given bit offset */
  FORCE_INLINE void
  rseek(size_t offset)
  {
    UINT n = offset % wsize;
    ptr_ = begin_ + offset/wsize;
    if (n) {
      buffer_ = read_word() >> n;
      bits_ = wsize - n;
    }
    else {
      buffer_ = 0;
      bits_ = 0;
    }
  }

  /* position stream for writing at given bit offset */
  FORCE_INLINE void
  wseek(size_t offset)
  {
    UINT n = offset % wsize;
    ptr_ = begin_ + offset/wsize;
    if (n) {
      T buffer = *ptr_;
      buffer &= ((T)1<<n) - 1;
      buffer_ = buffer;
      bits_ = n;
    }
    else {
      buffer_ = 0;
      bits_ = 0;
    }
  }

  /* skip over the next n bits (n >= 0) */
  FORCE_INLINE void
  skip(uint n)
  {
    rseek(rtell()+n);
  }

  /* append n zero bits to stream (n >= 0) */
  FORCE_INLINE void
  pad(uint n)
  {
    for (bits_ += n; bits_ >= wsize; bits_ -= wsize) {
      write_word(buffer_);
      buffer_ = 0;
    }
  }

  /* align stream on next word boundary */
  FORCE_INLINE void
  align()
  {
    if (bits_) {
      skip(bits_);
    }
  }

  /* write any remaining buffered bits and align stream on next word boundary */
  FORCE_INLINE void
  flush()
  {
    if (bits_) {
      pad(wsize-bits_);
    }
  }
};
