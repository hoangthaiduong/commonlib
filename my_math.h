#pragma once

#include "bit_ops.h"
#include "macros.h"
#include "vector.h"
#include "template.h"
#include <cstdint>
#include <cmath>
#include <type_traits>

constexpr double pi_ = 3.1415926535897932;

template <typename T>
FORCE_INLINE T
sqr(T v)
{
  return v*v;
}

/* Find the (floored) integer log base 2 of the input */
template <typename T, typename = enable_if_int<T>>
FORCE_INLINE int8_t
logi2(T v)
{
  REQUIRE(v > 0);
  return bsr(SCAST<std::make_unsigned_t<T>>(v));
}

/* Find the (floored) integer log base 4 of the input */
template <typename T, typename = enable_if_int<T>>
FORCE_INLINE int8_t
logi4(T v)
{
  REQUIRE(v > 0);
  return logi2(v)>>1;
}

/* Determine if the input is a power of two integer */
template <typename T, typename = enable_if_int<T>>
FORCE_INLINE bool
is_pow2(T v)
{
  REQUIRE(v > 0);
  return v && !(v&(v-1));
}

/* Transpose an NxM matrix to an MxN matrix */
template <typename T>
void
transpose(const T* input, int N, int M, OUT T* output)
{
  for (int n = 0; n < N; ++n) {
    for (int m = 0; m < M; ++m) {
      output[m*N+n] = input[n*M+m];
    }
  }
}

template <typename T>
FORCE_INLINE T
clamp(T val, T min_val, T max_val)
{
  REQUIRE(min_val <= max_val);
  if (val < min_val) {
    return min_val;
  }
  else if (val > max_val) {
    return max_val;
  }
  return val;
}

/* Note: here we assume the first and last point are the same */
template <typename T>
double
polygon_area(const Point2<T>* p, int n)
{
  REQUIRE(p[0] == p[n-1]);
  double area = 0;
  if (n < 4) {
    return 0;
  }
  for (int i = 0; i+1 < n; ++i) {
    area += p[i].x*p[i+1].y - p[i].y*p[i+1].x;
  }
  return std::abs(area) * 0.5;
}
