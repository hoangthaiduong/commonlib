#pragma once

#include "macros.h"
#include <cstdint>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

/* Read a binary raw file from disk */
template <typename T>
void
read_raw(const char* file_name, int nx, int ny, int nz, OUT T* out)
{
  THREAD_LOCAL static char buf[65536];
  std::ifstream fs;
  fs.rdbuf()->pubsetbuf(buf, sizeof(buf));
  fs.open(file_name, std::ios_base::binary);
  if (!fs) {
    throw std::ios_base::failure("file not found");
  };
  fs.seekg(0, std::ios_base::end);
  auto size = fs.tellg();
  if (CSIZE_T(size) != (nx*(ny*(nz*sizeof(T))))) {
    throw std::runtime_error("sizes do not match");
  }
  fs.seekg(0, std::ios_base::beg);
  fs.read(RCAST<char*>(out), size);
  if (!fs) {
    throw std::ios_base::failure("file cannot be read entirely");
  }
}

/* Write a binary raw file to disk */
template <typename T>
void
write_raw(const char* file_name, const T* f, int nelems)
{
  THREAD_LOCAL static char buf[65536];
  std::ofstream fs;
  fs.rdbuf()->pubsetbuf(buf, sizeof(buf));
  fs.open(file_name, std::ios_base::binary);
  if (!fs) {
    throw std::ios_base::failure("file cannot be opened");
  };
  fs.write(RCAST<const char*>(f), nelems*sizeof(T));
  if (!fs) {
    throw std::ios_base::failure("file cannot be written entirely");
  }
}

/* Read a text file into memory as an std::string */
std::string
read_file(const char *file_name)
{
  THREAD_LOCAL static char buf[65536];
  std::ifstream fs;
  fs.rdbuf()->pubsetbuf(buf, sizeof(buf));
  fs.open(file_name, std::ios::binary);
  if (!fs) {
    throw std::ios_base::failure("file cannot be opened");
  };
  std::string contents;
  fs.seekg(0, std::ios::end);
  contents.resize(fs.tellg());
  fs.seekg(0, std::ios::beg);
  fs.read(&contents[0], contents.size());
  if (!fs) {
    throw std::ios_base::failure("file cannot be read entirely");
  }
  return contents;
}

/* Write the values in a vector to a text file */
template <typename T>
void
write_values(const T* values, int n, const char* file_name)
{
  THREAD_LOCAL static char buf[65536];
  std::ofstream fs;
  fs.rdbuf()->pubsetbuf(buf, sizeof(buf));
  fs.open(file_name);
  if (!fs) {
    throw std::ios_base::failure("file cannot be opened");
  }
  for (int i = 0; i < n; ++i) {
    fs << values[i] << "\n";
  }
}

/* Print values in a vector for debugging */
template <typename T>
void
print_values(const std::vector<T>& values, char delim)
{
  for (const auto& v : values) {
    std::cout << values << delim;
  }
  std::cout << "\n";
}
