#pragma once

#include "bit_ops.h"
#include "macros.h"
#include "my_math.h"
#include "template.h"
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <type_traits>
#include <utility>
#include <vector>

/* Compute the squared error between two functions */
template <typename T>
double
squared_error(const T* u, const T* v, int64_t size)
{
  double err = 0;
  for (int64_t i = 0; i < size; ++i) {
    double diff = CDOUBLE(v[i]-u[i]);
    err += diff * diff;
  }
  return err;
}

template <typename T>
FORCE_INLINE double
rmse(const T* u, const T* v, int64_t size)
{
  return sqrt(squared_error(u, v, size)/size);
}

/* Compute the PSNR between two functions */
template <typename T>
double
psnr(const T* u, const T* v, int64_t size)
{
  double err = squared_error(u, v, size);
  auto mm = std::minmax_element(u, u+size);
  double d = 0.5 * (*(mm.second)-*(mm.first));
  err /= size;
  return 20.0 * log10(d) - 10.0 * log10(err);
}

/* Convert an integer to nega-binary */
template <typename T, typename = enable_if_int<T>>
FORCE_INLINE std::make_unsigned_t<T>
convert_negabinary(T v)
{
  using UT = std::make_unsigned_t<T>;
  auto mask = IntTraits<UT>::nbmask;
  return static_cast<UT>((v+mask)^mask);
}

template <typename T, typename = enable_if_int<T>>
void
convert_negabinary(
  const T* RESTRICT f_in, int64_t nelems, OUT std::make_unsigned_t<T>* RESTRICT f_out)
{
  for (int64_t i = 0; i < nelems; ++i) {
    f_out[i] = convert_negabinary(f_in[i]);
  }
}

/* Convert an integer to two's complement */
template <
  typename T, typename = enable_if_unsigned<T>>
FORCE_INLINE std::make_signed_t<T>
unconvert_negabinary(T v)
{
  auto mask = IntTraits<T>::nbmask;
  return static_cast<T>((v^mask)-mask);
}

template <
  typename T1, typename T2, typename = enable_if_unsigned<T1>, typename = enable_if_int<T2>>
void
unconvert_negabinary(const T1* RESTRICT f_in, int64_t nelems, OUT T2* RESTRICT f_out)
{
  for (int64_t i = 0; i < nelems; ++i) {
    f_out[i] = unconvert_negabinary<T1, T2>(f_in[i]);
  }
}

/* Take the dot product of two 1D signals */
//template <
//  typename T1, typename T2, typename = enable_if_float<T1>, typename = enable_if_float<T2>>
//double
//dot(const T1* f, int sf, const T2* g, int sg, int n)
//{
//  using namespace concurrency;
//  combinable<double> p([] { return 0; });
//  int stride = n / 8;
//  parallel_for(0, n, stride,
//    [&p, stride, f, g](int start) {
//    double p_local = 0;
//    for (int i = start; i < start+stride; ++i) {
//      p_local += f[i] * g[i];
//    }
//    p.local() += p_local;
//  });
//  return p.combine(std::plus<double>());
//}

template <
  typename T1, typename T2, typename = enable_if_float<T1>, typename = enable_if_float<T2>>
double
dot(const T1* f, int sf, const T2* g, int sg, int n)
{
  double p = 0;
  for (int i = 0; i < n; ++i) {
    p += f[i*sf] * g[i*sg];
  }
  return p;
}

/* Check a floating point data set for NaN and Inf */
template <typename T, typename = enable_if_float<T>>
bool
check_nan_inf(const T* f, int64_t size)
{
  for (int64_t i = 0; i < size; ++i) {
    if (!isfinite(f[i])) {
      return false;
    }
  }
  return true;
}

/* Keep some number of the most significant bits in an array, assuming the values are
stored in two's complement binary format */
template <typename T, typename = enable_if_int<T>>
void
reduce_precision(const T* RESTRICT f, int64_t size, int bits, OUT T* RESTRICT g)
{
  /* convert f to sign-magnitude */
  using UT = std::make_unsigned_t<T>;
  int w = CHAR_BIT * sizeof(UT);
  int first_b = -1;
  for (int64_t i = 0; i < size; ++i) {
    T v = f[i];
    int s = v < 0;
    int t = (1-s)*2 - 1; // map (0, 1) to (1, -1)
    g[i] = (UT(s)<<(w-1)) + t*v;
    first_b = std::max(first_b, int(bsr(SCAST<UT>(t*v))));
  }

  /* reduce precision and convert back to two's complement */
  UT mask = (UT(1)<<(w-1));
  for (int i = 0; i < bits; ++i) {
    mask |= (UT(1)<<std::max(first_b-i, 0));
  }
  for (int64_t i = 0; i < size; ++i) {
    int s = g[i] < 0;
    int t = (1-s)*2 - 1; // map (0, 1) to (1, -1)
    g[i] &= mask;
    g[i] = t * (g[i]-(UT(s)<<(w-1)));
  }
}

/* Keep some number of the most significant bits in an array, assuming the values are
stored in negabinary format */
template <typename T, typename = enable_if_unsigned<T>>
void
reduce_precision_negabinary(
  const T* RESTRICT f, int64_t size, int maxbits, int bits, OUT T* RESTRICT g)
{
  int first_b = -1;
  for (int64_t i = 0; i < size; ++i) {
    first_b = std::max(first_b, int(bsr(f[i])));
  }
  int shift = first_b-std::min(bits, maxbits)+1;
  for (int64_t i = 0; i < size; ++i) {
    g[i] = (f[i]>>shift) << shift;
  }
}

/* Convolve two 1D signals. The size of the output signal should be nf+ng-1. */
template <typename T>
void
convolve(const T* RESTRICT f, int nf, const T* RESTRICT g, int ng, OUT T* RESTRICT y)
{
  int ny = nf + ng - 1;
  // y[n] = \sum f[k] * g[n-k]
  PARALLEL_LOOP
  for (int n = 0; n < ny; ++n) {
    y[n] = 0;
    for (int k = 0; k < nf; ++k) {
      if (n>=k && n<ng+k) {
        y[n] += f[k] * g[n-k];
      }
    }
  }
}

/* Convenient wrapper around the above convolve. */
template <typename T>
std::vector<T>
convolve(const T* f, int nf, const T* g, int ng)
{
  int ny = nf + ng - 1;
  std::vector<T> y(ny);
  // y[n] = \sum f[k] * g[n-k]
  PARALLEL_LOOP
  for (int n = 0; n < ny; ++n) {
    y[n] = 0;
    for (int k = 0; k < nf; ++k) {
      if (n>=k && n<ng+k) {
        y[n] += f[k] * g[n-k];
      }
    }
  }
  return y;
}

/* For a 1D signal, insert a zero (odd) sample between every two adjacent even samples.
The size of the output signal should be nf*2-1. */
template <typename T>
void
upsample_zeros(const T* RESTRICT f, int nf, OUT T* RESTRICT g)
{
  int ng = nf*2 - 1;
  for (int i = 0; i < ng; i += 2) {
    g[i] = f[i/2];
    if (i+1 < ng) {
      g[i+1] = 0;
    }
  }
}

/* Convenient wrapper around the above upsample_zeros. */
template <typename T>
std::vector<T>
upsample_zeros(const T* f, int nf)
{
  int ng = nf*2 - 1;
  std::vector<T> g(ng);
  upsample_zeros(f, nf, g.data());
  return g;
}

/* Compute the squared norm of an nD signal */
template <typename T>
double
sqr_norm(const T* f, int64_t nf)
{
  double norm = 0;
  for (int64_t i = 0; i < nf; ++i) {
    norm += f[i] * f[i];
  }
  return norm;
}

template <typename T>
FORCE_INLINE double
norm(const T* f, int64_t nf)
{
  return sqrt(sqr_norm(f, nf));
}

/* Return normalized floating-point exponent for x >= 0 */
template <typename T, typename = enable_if_float<T>>
FORCE_INLINE int
exponent(T x)
{
  if (x > 0) {
    int e;
    frexp(x, &e);
    /* clamp exponent in case x is denormal */
    return std::max(e, 1-FloatTraits<T>::ebias);
  }
  return -FloatTraits<T>::ebias;
}

/* Map floating-point number x to integer relative to exponent e */
FORCE_INLINE double
quantize(double x, int e, int bits)
{
  return ldexp(x, bits-e);
}

/* Forward block-floating-point transform to signed integers, returning the max exponent */
template <typename T1, typename T2, typename = enable_if_float<T1>, typename = enable_if_int<T2>>
int
quantize(const T1* RESTRICT f, int64_t n, int bits, OUT T2* RESTRICT fi)
{
  /* compute maximum exponent in the function */
  T1 max = *(std::max_element(f, f+n, [](T1 a, T1 b) { return std::abs(a) < std::abs(b); }));
  int emax = exponent(std::abs(max));
  /* compute power-of-two scale factor s */
  double s = quantize(1, emax, bits);
  /* compute p-bit int y = s*x where x is floating and |y| <= 2^p-1 */
  for (int64_t i = 0; i < n; ++i) {
    fi[i] = SCAST<T2>(s*f[i]);
  }
  return emax;
}

template <typename T1, typename T2, typename = enable_if_float<T1>, typename = enable_if_int<T2>>
void
dequantize(const T2* RESTRICT fi, int64_t n, int emax, int bits, OUT T1* RESTRICT f)
{
  double s = 1.0 / quantize(1, emax, bits);
  for (int64_t i = 0; i < n; ++i) {
    f[i] = SCAST<T1>(s*fi[i]);
  }
}

template <typename T>
void
histogram(const T* f, int64_t n, T fmin, T fmax, OUT VecInt64* hist)
{
  int nbins = CINT(hist->size());
  REQUIRE(nbins > 1);
  std::fill(hist->begin(), hist->end(), 0);
  T delta = (fmax-fmin) / (nbins-1);
  if (delta != 0) {
    T start = fmin - delta/2;
    for (int64_t i = 0; i < n; ++i) {
      int bin = CINT((f[i]-start)/delta);
      bin = clamp(bin, 0, nbins-1);
      ++(*hist)[bin];
    }
  }
}

/* Compute the histogram for a sub-block of the whole data (nx, ny, nz) */
template <typename T>
void
histogram(
  const T* f, int nx, int ny, int nz, int fromx, int fromy, int fromz, int tox, int toy, int toz,
  T fmin, T fmax, OUT VecInt64* hist)
{
  (void)nz;
  int nbins = CINT(hist->size());
  REQUIRE(nbins > 1);
  std::fill(hist->begin(), hist->end(), 0);
  T delta = (fmax-fmin) / (nbins-1);
  if (delta != 0) {
    T start = fmin - delta/2;
    for (int z = fromz; z < toz; ++z) {
      for (int y = fromy; y < toy; ++y) {
        for (int x = fromx; x < tox; ++x) {
          int i = z*nx*ny + y*nx + x;
          int bin = CINT((f[i]-start)/delta);
          bin = clamp(bin, 0, nbins-1);
          //REQUIRE(0<=bin && bin<nbins);
          ++(*hist)[bin];
        }
      }
    }
  }
}

FORCE_INLINE double
histogram_diff(const VecInt64& h1, const VecInt64& h2)
{
  return squared_error(h1.data(), h2.data(), CINT64(h1.size()));
}

template <typename F>
void
histogram_op(const VecInt64& h1, const VecInt64& h2, F op, OUT VecInt64* hout)
{
  int n = CINT(h1.size());
  REQUIRE(n==h2.size() && n==hout->size());
  for (int i = 0; i < n; ++i) {
    (*hout)[i] = op(h1[i], h2[i]);
  }
}

/* Compute the average and standard deviation */
template <typename T>
std::pair<double, double>
avg_stddev(const T* f, int64_t n)
{
  double avg = 0;
  for (int64_t i = 0; i < n; ++i) {
    avg += f[i];
  }
  avg /= n;
  double stddev = 0;
  for (int64_t i = 0; i < n; ++i) {
    stddev += (f[i]-avg)*(f[i]-avg);
  }
  stddev /= n;
  return std::make_pair(avg, sqrt(stddev));
}
