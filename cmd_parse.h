#pragma once

#include "macros.h"
#include <algorithm>
#include <cstring>
#include <exception>
#include <stdexcept>
#include <string>
#include <utility>

/* Get a list of option arguments, given the number of arguments following the
given option */
inline std::pair<char**, char**>
get_opt_args(char** start, char** end, const char* option, int num_args)
{
  REQUIRE(num_args > 0);
  auto itr = std::find_if(start, end,
    [option](const auto entry) { return strcmp(option, entry) == 0; });
  if (itr == end) {
    throw std::runtime_error(std::string(option) + " not found");
  }
  if ((end-itr) <= num_args) {
    throw std::runtime_error("Not enough option arguments for " + std::string(option)
                             + " (need " + std::to_string(num_args) + ")");
  }
  return std::make_pair(itr+1, itr+1+num_args);
}

/* Get a list of option arguments, given that each option is specified using a
prefix such as --help */
inline std::pair<char**, char**>
get_opt_args(char** start, char** end, const char* option, const char* prefix)
{
  auto itr_start = std::find_if(start, end,
    [option](const auto entry) { return strcmp(option, entry) == 0; });
  if (itr_start == end) {
    throw std::runtime_error(std::string(option) + " not found");
  }
  auto itr_end = std::find_if(itr_start+1, end,
    [option, prefix](const auto entry) { return strstr(entry, prefix) != nullptr; });
  return std::make_pair(itr_start+1, itr_end);
}

/* Get the (one) argument following the given option */
inline const char*
get_opt_arg(char** start, char** end, const char* option)
{
  auto result = get_opt_args(start, end, option, 1);
  return *(result.first);
}

/* Return whether a given option exists */
inline bool
get_opt(char** start, char** end, const char* option)
{
  auto itr = std::find_if(start, end,
    [option](const auto entry) { return strcmp(option, entry) == 0; });
  return !(itr==end);
}
