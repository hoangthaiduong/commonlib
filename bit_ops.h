#pragma once

#include <macros.h>
#include <cstdint>

/* Return the bit plane of the most significant one-bit. Counting starts from
the least significant bit plane.
Examples: bsr(0)=-1, bsr(2)= 1, bsr(5)=2, bsr(8)=3 */
#if defined(_MSC_VER)
#include <intrin.h>
#pragma intrinsic(_BitScanReverse)
#pragma intrinsic(_BitScanReverse64)
FORCE_INLINE int8_t
bsr(unsigned int v)
{
  unsigned long index = 0;
  _BitScanReverse(&index, v);
  return v==0 ? -1 : static_cast<int8_t>(index);
}
FORCE_INLINE int8_t
bsr(unsigned __int64 v)
{
  unsigned long index = 0;
  _BitScanReverse64(&index, v);
  return v==0 ? -1 : static_cast<int8_t>(index);
}
#else
FORCE_INLINE int8_t
bsr(unsigned int v)
{
  return v==0 ? -1 : static_cast<int8_t>(sizeof(unsigned int)*CHAR_BIT-1-__builtin_clz(v));
}
FORCE_INLINE int8_t
bsr(unsigned long long v)
{
  return v==0 ? -1 : static_cast<int8_t>(sizeof(unsigned long long)*CHAR_BIT-1-__builtin_clzll(v));
}
#endif

/* Reverse the operation that inserts two 0 bits after every bit of x */
FORCE_INLINE uint32_t
compact_1_by_2(uint32_t x)
{
  x &= 0x09249249;              // x = ---- 9--8 --7- -6-- 5--4 --3- -2-- 1--0
  x = (x^(x>> 2)) & 0x030c30c3; // x = ---- --98 ---- 76-- --54 ---- 32-- --10
  x = (x^(x>> 4)) & 0x0300f00f; // x = ---- --98 ---- ---- 7654 ---- ---- 3210
  x = (x^(x>> 8)) & 0xff0000ff; // x = ---- --98 ---- ---- ---- ---- 7654 3210
  x = (x^(x>>16)) & 0x000003ff; // x = ---- ---- ---- ---- ---- --98 7654 3210
  return x;
}

/* Morton decoding */
FORCE_INLINE uint32_t
decode_morton3_x(uint32_t code)
{
  return compact_1_by_2(code >> 0);
}
FORCE_INLINE uint32_t
decode_morton3_y(uint32_t code)
{
  return compact_1_by_2(code >> 1);
}
FORCE_INLINE uint32_t
decode_morton3_z(uint32_t code)
{
  return compact_1_by_2(code >> 2);
}
