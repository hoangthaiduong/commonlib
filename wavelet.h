#pragma once

#include "bit_ops.h"
#include "function.h"
#include "macros.h"
#include "my_math.h"
#include <algorithm>
#include <cstdint>
#include <mutex>
#include <utility>
#include <tuple>
#include <vector>

std::mutex get_info_mutex_;

/*----------------------------------------------------------------
CDF 5/3 wavelets
----------------------------------------------------------------*/

/* Compute the squared norms of the CDF5/3 scaling and wavelet functions.
nlevels is the number of times the wavelet transform is done (it is the number of levels
minus one). The first two elements are the scaling norms and wavelet norms, the last two
are the lengths of the scaling and wavelet functions. */
std::tuple<const VecDouble*, const VecDouble*, const VecInt*, const VecInt*>
get_cdf53_info()
{
  constexpr int nlevels = 6;
  static VecDouble wavelet_norms;
  static VecDouble scaling_norms;
  static VecInt scaling_lengths;
  static VecInt wavelet_lengths;
  if (wavelet_norms.size() != nlevels+1) {
    std::unique_lock<std::mutex> lock(get_info_mutex_);
    VecDouble scaling_weights = {0.5, 1.0, 0.5};
    VecDouble scaling_func    = {0.5, 1.0, 0.5};
    VecDouble wavelet_weights = {-0.125, -0.25, 0.75, -0.25, -0.125};
    VecDouble wavelet_func    = {-0.125, -0.25, 0.75, -0.25, -0.125};

    wavelet_norms.resize(nlevels+1);
    scaling_norms.resize(nlevels+1);
    scaling_lengths.resize(nlevels+1);
    wavelet_lengths.resize(nlevels+1);
    std::vector<VecDouble> wavelet_funcs(nlevels+1);
    /* compute the squared norms of each scaling and wavelet function */
    for (int l = 0; l < nlevels+1; ++l) {
      scaling_norms[l] = norm(scaling_func.data(), scaling_func.size());
      scaling_lengths[l] = CINT(scaling_func.size());
      wavelet_norms[l] = norm(wavelet_func.data(), wavelet_func.size());
      wavelet_lengths[l] = CINT(wavelet_func.size());
      // upsample the wavelet weights
      wavelet_weights = upsample_zeros(wavelet_weights.data(), CINT(wavelet_weights.size()));
      // compute the new wavelet function
      wavelet_func = convolve(wavelet_weights.data(), CINT(wavelet_weights.size()), scaling_func.data(), CINT(scaling_func.size()));
      // upsample the scaling weights
      scaling_weights = upsample_zeros(scaling_weights.data(), CINT(scaling_weights.size()));
      // compute the new scaling function
      scaling_func = convolve(scaling_weights.data(), CINT(scaling_weights.size()), scaling_func.data(), CINT(scaling_func.size()));
    }
  }
  return std::make_tuple(&scaling_norms, &wavelet_norms, &scaling_lengths, &wavelet_lengths);
}

std::tuple<const VecDouble*, const VecDouble*, const VecInt*, const VecInt*>
get_cdf97_info()
{
  constexpr int nlevels = 6;
  static VecDouble wavelet_norms;
  static VecDouble scaling_norms;
  static VecInt scaling_lengths;
  static VecInt wavelet_lengths;

  if (wavelet_norms.size() != nlevels+1) {
    std::unique_lock<std::mutex> lock(get_info_mutex_);
    VecDouble scaling_weights = {-0.064538882683548862,-0.040689417645525548,0.41809227288199591,0.78848561698464381,0.41809227288199591,-0.040689417645525548,-0.064538882683548862};
    VecDouble scaling_func = scaling_weights;
    VecDouble wavelet_weights = {0.03782845549569927, 0.023849465013159186, -0.11062440408581103, -0.37740285551263303, 0.85269867883697925, -0.37740285551263303, -0.11062440408581103, 0.023849465013159186, 0.03782845549569927};
    VecDouble wavelet_func = wavelet_weights;

    wavelet_norms.resize(nlevels+1);
    scaling_norms.resize(nlevels+1);
    scaling_lengths.resize(nlevels+1);
    wavelet_lengths.resize(nlevels+1);
    std::vector<VecDouble> wavelet_funcs(nlevels+1);
    /* compute the squared norms of each scaling and wavelet function */
    for (int l = 0; l < nlevels+1; ++l) {
      scaling_norms[l] = norm(scaling_func.data(), scaling_func.size());
      scaling_lengths[l] = CINT(scaling_func.size());
      wavelet_norms[l] = norm(wavelet_func.data(), wavelet_func.size());
      wavelet_lengths[l] = CINT(wavelet_func.size());
      // upsample the wavelet weights
      wavelet_weights = upsample_zeros(wavelet_weights.data(), CINT(wavelet_weights.size()));
      // compute the new wavelet function
      wavelet_func = convolve(wavelet_weights.data(), CINT(wavelet_weights.size()), scaling_func.data(), CINT(scaling_func.size()));
      // upsample the scaling weights
      scaling_weights = upsample_zeros(scaling_weights.data(), CINT(scaling_weights.size()));
      // compute the new scaling function
      scaling_func = convolve(scaling_weights.data(), CINT(scaling_weights.size()), scaling_func.data(), CINT(scaling_func.size()));
    }
  }
  return std::make_tuple(&scaling_norms, &wavelet_norms, &scaling_lengths, &wavelet_lengths);
}

/* Forward X lifting */
// TODO: deal with non-power-of-two dimensions
template <typename T>
void
flift_cdf53_x(T* f, int nx, int ny, int nz, int lx, int ly, int lz, bool normalize=false)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mx <= 1) {
    return;
  }
  double s = 1, w = 1;
  if (normalize) {
    auto cdf53 = get_cdf53_info();
    const VecDouble& sweights = *std::get<0>(cdf53);
    const VecDouble& wweights = *std::get<1>(cdf53);
    s = sweights[lx] / (lx>0?sweights[lx-1]:1);
    w = wweights[lx] / (lx>0?sweights[lx-1]:1);
  }

  // predict
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        T fleft  =          f[z*nx*ny+y*nx+(x-1)];
        T fright = x<mx-1 ? f[z*nx*ny+y*nx+(x+1)] : fleft;
        f[z*nx*ny+y*nx+x] -= (fleft+fright) / 2;
      }
    }
  }

  // update
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        int xleft = x - 1;
        int xright = x<mx-1 ? x+1 : x-1;
        f[z*nx*ny+y*nx+xleft ] += f[z*nx*ny+y*nx+x] / 4;
        f[z*nx*ny+y*nx+xright] += f[z*nx*ny+y*nx+x] / 4;
      }
    }
  }

  // pack
  THREAD_LOCAL static std::vector<T> t; t.resize(mx>>1);
  int sx = (mx+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        t[x>>1]                = SCAST<T>(f[z*nx*ny+y*nx+ x   ] * w); // odd
        f[z*nx*ny+y*nx+(x>>1)] = SCAST<T>(f[z*nx*ny+y*nx+(x-1)] * s); // even
      }
      if ((mx&1) != 0) { // last even
        f[z*nx*ny+y*nx+(mx>>1)] = SCAST<T>(f[z*nx*ny+y*nx+(mx-1)] * s);
      }
      for (int x = 0; x < (mx>>1); ++x) {
        f[z*nx*ny+y*nx+(sx+x)] = t[x];
      }
    }
  }
}

/* Inverse X lifting */
// TODO: deal with non-power-of-two dimensions
template <typename T>
void
ilift_cdf53_x(T* f, int nx, int ny, int nz, int lx, int ly, int lz, bool normalize=false)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mx <= 1) {
    return;
  }
  double s = 1, w = 1;
  if (normalize) {
    auto cdf53 = get_cdf53_info();
    const VecDouble& sweights = *std::get<0>(cdf53);
    const VecDouble& wweights = *std::get<1>(cdf53);
    s = (lx>0?sweights[lx-1]:1) / sweights[lx];
    w = (lx>0?sweights[lx-1]:1) / wweights[lx];
  }

  // unpack
  THREAD_LOCAL static std::vector<T> t; t.resize(mx>>1);
  int sx = (mx+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 0; x < (mx>>1); ++x) {
        t[x] = f[z*nx*ny+y*nx+(sx+x)];
      }
      for (int x = (mx>>1)*2-1; x >= 1; x -= 2) {
        f[z*nx*ny+y*nx+(x-1)] = SCAST<T>(f[z*nx*ny+y*nx+(x>>1)] * s); // even
        f[z*nx*ny+y*nx+ x   ] = SCAST<T>(t[x>>1] * w); // odd
      }
      if ((mx&1) != 0) { // last even
        f[z*nx*ny+y*nx+(mx-1)] = SCAST<T>(f[z*nx*ny+y*nx+(mx>>1)] * s);
      }
    }
  }

  // inverse update
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        int xleft = x - 1;
        int xright = x<mx-1 ? x+1 : x-1;
        f[z*nx*ny+y*nx+xleft ] -= f[z*nx*ny+y*nx+x] / 4;
        f[z*nx*ny+y*nx+xright] -= f[z*nx*ny+y*nx+x] / 4;
      }
    }
  }

  // inverse predict
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        T fleft  =          f[z*nx*ny+y*nx+(x-1)];
        T fright = x<mx-1 ? f[z*nx*ny+y*nx+(x+1)] : fleft;
        f[z*nx*ny+y*nx+x] += (fleft+fright) / 2;
      }
    }
  }
}

/* Forward Y lifting */
// TODO: deal with non-power-of-two dimensions
template <typename T>
void
flift_cdf53_y(T* f, int nx, int ny, int nz, int lx, int ly, int lz, bool normalize=false)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (my <= 1) {
    return;
  }
  double s = 1, w = 1;
  if (normalize) {
    auto cdf53 = get_cdf53_info();
    const VecDouble& sweights = *std::get<0>(cdf53);
    const VecDouble& wweights = *std::get<1>(cdf53);
    s = sweights[ly] / (ly>0?sweights[ly-1]:1);
    w = wweights[ly] / (ly>0?sweights[ly-1]:1);
  }
  // predict
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        T fleft  =          f[z*nx*ny+(y-1)*nx+x];
        T fright = y<my-1 ? f[z*nx*ny+(y+1)*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] -= (fleft+fright) / 2;
      }
    }
  }

  // update
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        int yleft = y - 1;
        int yright = y<my-1 ? y+1 : y-1;
        f[z*nx*ny+yleft *nx+x] += f[z*nx*ny+y*nx+x] / 4;
        f[z*nx*ny+yright*nx+x] += f[z*nx*ny+y*nx+x] / 4;
      }
    }
  }

  // pack
  THREAD_LOCAL static std::vector<T> t; t.resize(my>>1);
  int sy = (my+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        t[y>>1]                = SCAST<T>(f[z*nx*ny+ y   *nx+x] * w); // odd
        f[z*nx*ny+(y>>1)*nx+x] = SCAST<T>(f[z*nx*ny+(y-1)*nx+x] * s); // even
      }
      if ((my&1) != 0) {
        f[z*nx*ny+(my>>1)*nx+x] = SCAST<T>(f[z*nx*ny+(my-1)*nx+x] * s);
      }
      for (int y = 0; y < (my>>1); ++y) {
        f[z*nx*ny+(sy+y)*nx+x] = t[y];
      }
    }
  }
}

/* Inverse Y lifting */
// TODO: deal with non-power-of-two dimensions
template <typename T>
void
ilift_cdf53_y(T* f, int nx, int ny, int nz, int lx, int ly, int lz, bool normalize=false)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (my <= 1) {
    return;
  }
  double s = 1, w = 1;
  if (normalize) {
    auto cdf53 = get_cdf53_info();
    const VecDouble& sweights = *std::get<0>(cdf53);
    const VecDouble& wweights = *std::get<1>(cdf53);
    s = (ly>0?sweights[ly-1]:1) / sweights[ly];
    w = (ly>0?sweights[ly-1]:1) / wweights[ly];
  }

  // unpack
  THREAD_LOCAL static std::vector<T> t; t.resize(my>>1);
  int sy = (my+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 0; y < (my>>1); ++y) {
        t[y] = f[z*nx*ny+(sy+y)*nx+x];
      }
      for (int y = (my>>1)*2-1; y >= 1; y -= 2) {
        f[z*nx*ny+(y-1)*nx+x] = SCAST<T>(f[z*nx*ny+(y>>1)*nx+x] * s); // even
        f[z*nx*ny+ y   *nx+x] = SCAST<T>(t[y>>1] * w); // odd
      }
      if ((my&1) != 0) { // last even
        f[z*nx*ny+(my-1)*nx+x] = SCAST<T>(f[z*nx*ny+(my>>1)*nx+x] * s);
      }
    }
  }

  // inverse update
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        int yleft = y - 1;
        int yright = y<my-1 ? y+1 : y-1;
        f[z*nx*ny+yleft *nx+x] -= f[z*nx*ny+y*nx+x] / 4;
        f[z*nx*ny+yright*nx+x] -= f[z*nx*ny+y*nx+x] / 4;
      }
    }
  }

  // inverse predict
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        T fleft  =          f[z*nx*ny+(y-1)*nx+x];
        T fright = y<my-1 ? f[z*nx*ny+(y+1)*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += (fleft+fright) / 2;
      }
    }
  }
}

/* Forward Z lifting */
// TODO: deal with non-power-of-two dimensions
template <typename T>
void
flift_cdf53_z(T* f, int nx, int ny, int nz, int lx, int ly, int lz, bool normalize=false)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mz <= 1) {
    return;
  }
  double s = 1, w = 1;
  if (normalize) {
    auto cdf53 = get_cdf53_info();
    const VecDouble& sweights = *std::get<0>(cdf53);
    const VecDouble& wweights = *std::get<1>(cdf53);
    s = sweights[lz] / (lz>0?sweights[lz-1]:1);
    w = wweights[lz] / (lz>0?sweights[lz-1]:1);
  }

  // predict
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        T fleft  =          f[(z-1)*nx*ny+y*nx+x];
        T fright = z<mz-1 ? f[(z+1)*nx*ny+y*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] -= (fleft+fright) / 2;
      }
    }
  }

  // update
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        int zleft  = z - 1;
        int zright = z<mz-1 ? z+1 : z-1;
        f[zleft *nx*ny+y*nx+x] += f[z*nx*ny+y*nx+x] / 4;
        f[zright*nx*ny+y*nx+x] += f[z*nx*ny+y*nx+x] / 4;
      }
    }
  }

  // pack
  THREAD_LOCAL static std::vector<T> t; t.resize(mz>>1);
  int sz = (mz+1) >> 1;
  for (int y = 0; y < my; ++y) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        t[z>>1]                = SCAST<T>(f[ z   *nx*ny+y*nx+x] * w); // odd
        f[(z>>1)*nx*ny+y*nx+x] = SCAST<T>(f[(z-1)*nx*ny+y*nx+x] * s); // even
      }
      if ((mz&1) != 0) {
        f[(mz>>1)*nx*ny+y*nx+x] = SCAST<T>(f[(mz-1)*nx*ny+y*nx+x] * s);
      }
      for (int z = 0; z < (mz>>1); ++z) {
        f[(sz+z)*nx*ny+y*nx+x] = t[z];
      }
    }
  }
}

/* Inverse Z lifting */
// TODO: deal with non-power-of-two dimensions
template <typename T>
void
ilift_cdf53_z(T* f, int nx, int ny, int nz, int lx, int ly, int lz, bool normalize=true)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mz <= 1) {
    return;
  }
  double s = 1, w = 1;
  if (normalize) {
    auto cdf53 = get_cdf53_info();
    const VecDouble& sweights = *std::get<0>(cdf53);
    const VecDouble& wweights = *std::get<1>(cdf53);
    s = (lz>0?sweights[lz-1]:1) / sweights[lz];
    w = (lz>0?sweights[lz-1]:1) / wweights[lz];
  }

  // unpack
  THREAD_LOCAL static std::vector<T> t; t.resize(mz>>1);
  for (int y = 0; y < my; ++y) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      int sz = (mz+1) >> 1;
      for (int z = 0; z < (mz>>1); ++z) {
        t[z] = f[(sz+z)*nx*ny+y*nx+x];
      }
      for (int z = (mz>>1)*2-1; z >= 1; z -= 2) {
        f[(z-1)*nx*ny+y*nx+x] = SCAST<T>(f[(z>>1)*nx*ny+y*nx+x] * s); // even
        f[ z   *nx*ny+y*nx+x] = SCAST<T>(t[z>>1] * w); // odd
      }
      if ((mz&1) != 0) {
        f[(mz-1)*nx*ny+y*nx+x] = SCAST<T>(f[(mz>>1)*nx*ny+y*nx+x] * s);
      }
    }
  }

  // inverse update
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        int zleft = z - 1;
        int zright = z<mz-1 ? z+1 : z-1;
        f[zleft *nx*ny+y*nx+x] -= f[z*nx*ny+y*nx+x] / 4;
        f[zright*nx*ny+y*nx+x] -= f[z*nx*ny+y*nx+x] / 4;
      }
    }
  }

  // inverse predict
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        T fleft  =          f[(z-1)*nx*ny+y*nx+x];
        T fright = z<mz-1 ? f[(z+1)*nx*ny+y*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += (fleft+fright) / 2;
      }
    }
  }
}

/* Keep the first few levels of wavelet transform and set the rest to 0 */
template <typename T>
void
reduce_resolution(
  T* RESTRICT f, int nx, int ny, int nz, int nlevels, int retain_nlevels, OUT T* RESTRICT g)
{
  int l = std::max(nlevels-retain_nlevels+1, 0);
  int mx = nx>>l, my = ny>>l, mz = std::max(nz>>l, 1);
  for (int z = 0; z < nz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < ny; ++y) {
      for (int x = 0; x < nx; ++x) {
        int i = z*nx*ny + y*nx+ x;
        if (z<mz && y<my && x<mx) {
          g[i] = f[i];
        }
        else {
          g[i] = 0;
        }
      }
    }
  }
}

/* From an index, return the wavelet level. The coarsest level is 0. nlevels is the number
of times the wavelet transform is recurred (i.e., the number of wavelet levels minus one.) */
FORCE_INLINE int
wavelet_level(int c, int n, int nlevels)
{
  int l = 0;
  while (l<nlevels && (n=((n+1)>>1))>c) {
    ++l;
  }
  return nlevels - l;
}

/* Return the range of indices corresponding to a wavelet level [from, to). The coarsest
level is 0. nlevels is the number of times the wavelet transform is recurred (i.e., the
number of wavelet levels minus one.) */
// TODO: only works for power of two dimension
FORCE_INLINE std::pair<int, int>
level_to_range(int l, int n, int nlevels)
{
  int from = 0, to = 1;
  from = l==0 ? 0 : n>>(nlevels-l+1);
  to   = n>>(nlevels-l);
  return std::make_pair(from, to);
}

/* Return from_x, from_y, level_x, level_y. For quad-tree style decomposition. */
std::tuple<int, int, int, int>
wavelet_level_xy(int cx, int cy, int nx, int ny, int nlevels)
{
  int bits_x = logi2(nx-1)+1, bits_y = logi2(ny-1)+1; // number of bits needed to store nx, ny
  //int shift_x = bits_x-nlevels, shift_y = bits_y-nlevels;
  int from_x = cx>>(bits_x-nlevels), from_y = cy>>(bits_y-nlevels);
  /* determine the level based on the first set bit */
  int lx = bsr(CUINT(from_x))+1, ly = bsr(CUINT(from_y))+1;
  int level = std::max(lx, ly);
  int mx = lx<level, my = ly<level;
  int shift_x = bits_x - std::min(nlevels-level+1, nlevels);
  int shift_y = bits_y - std::min(nlevels-level+1, nlevels);
  return std::make_tuple((cx>>shift_x)<<shift_x, (cy>>shift_y)<<shift_y, level-mx, level-my);
}

FORCE_INLINE int
level_xy_to_subband(int lx, int ly)
{
  if (lx+ly == 0) return 0;
  return 3*((lx+ly-1)>>1) + 2*(ly>=lx) + (lx>=ly);
}

/* Given an index for a wavelet coefficient, return the inclusive range [] of indices that
this coefficient influences (element 0 and 1), as well as the wavelet level (element 2).
The coarsest level is 0. nlevels is the number of times the wavelet transform is recurred
(i.e., the number of wavelet levels minus one.) */
std::tuple<int, int, int>
influence_range_cdf53(int c, int n, int nlevels)
{
  if (n == 1) {
    return std::make_tuple(0, 0, 0);
  }
  int l = wavelet_level(c, n, nlevels);
  int l_backup = l;
  auto cdf53 = get_cdf53_info();
  int length = l==0 ? (*std::get<2>(cdf53))[nlevels-1] : (*std::get<3>(cdf53))[nlevels-l];
  int from, to;
  std::tie(from, to) = level_to_range(l, n, nlevels);
  /* find the middle index */
  c -= from;
  if (l != 0) {
    c = c*2 + 1;
  }
  ++l;
  c <<= (nlevels-l+1);
  return std::make_tuple(std::max(0, c-length/2), std::min(n-1, c+length/2), l_backup);
}

std::tuple<int, int, int, int>
coeff_to_pos(int cx, int cy, int nx, int ny, int nlevels)
{
  int from_x, from_y, lx, ly;
  std::tie(from_x, from_y, lx, ly) = wavelet_level_xy(cx, cy, nx, ny, nlevels);
  int l = std::max(lx, ly);
  /* find the middle index */
  cx -= from_x; cy -= from_y;
  if (l != 0) {
    cx = cx*2 + (lx==l);
    cy = cy*2 + (ly==l);
  }
  cx <<= (nlevels-l);
  cy <<= (nlevels-l);
  return std::make_tuple(cx, cy, lx, ly);
}

/* For 2D. Return tuple <fromx, fromy, tox, toy, levelx, levely> */
std::tuple<int, int, int, int, int, int>
influence_range_cdf53(int cx, int cy, int nx, int ny, int nlevels)
{
  int from_x, from_y, lx, ly;
  std::tie(from_x, from_y, lx, ly) = wavelet_level_xy(cx, cy, nx, ny, nlevels);
  int l = std::max(lx, ly);
  int lx_backup = lx, ly_backup = ly;
  auto cdf53 = get_cdf53_info();
  int lengthx, lengthy;
  if (l == 0) { // low-low
    lengthx = lengthy = (*std::get<2>(cdf53))[nlevels-1];
  }
  else { // high-low or low-high or high-high
    lengthx = lx==l ? (*std::get<3>(cdf53))[nlevels-l] : (*std::get<2>(cdf53))[nlevels-l];
    lengthy = ly==l ? (*std::get<3>(cdf53))[nlevels-l] : (*std::get<2>(cdf53))[nlevels-l];
  }
  /* find the middle index */
  cx -= from_x; cy -= from_y;
  if (l != 0) {
    cx = cx*2 + (lx==l);
    cy = cy*2 + (ly==l);
  }
  cx <<= (nlevels-l);
  cy <<= (nlevels-l);
  return std::make_tuple(
    std::max(0, cx-lengthx/2), std::max(0, cy-lengthy/2),
    std::min(nx-1, cx+lengthx/2), std::min(ny-1, cy+lengthy/2),
    lx_backup, ly_backup);
}

std::tuple<int, int, int, int, int, int>
influence_range_cdf97(int cx, int cy, int nx, int ny, int nlevels)
{
  int from_x, from_y, lx, ly;
  std::tie(from_x, from_y, lx, ly) = wavelet_level_xy(cx, cy, nx, ny, nlevels);
  int l = std::max(lx, ly);
  int lx_backup = lx, ly_backup = ly;
  auto cdf97 = get_cdf97_info();
  int lengthx, lengthy;
  if (l == 0) { // low-low
    lengthx = lengthy = (*std::get<2>(cdf97))[nlevels-1];
  }
  else { // high-low or low-high or high-high
    lengthx = lx==l ? (*std::get<3>(cdf97))[nlevels-l] : (*std::get<2>(cdf97))[nlevels-l];
    lengthy = ly==l ? (*std::get<3>(cdf97))[nlevels-l] : (*std::get<2>(cdf97))[nlevels-l];
  }
  /* find the middle index */
  cx -= from_x; cy -= from_y;
  if (l != 0) {
    cx = cx*2 + (lx==l);
    cy = cy*2 + (ly==l);
  }
  cx <<= (nlevels-l);
  cy <<= (nlevels-l);
  return std::make_tuple(
    std::max(0, cx-lengthx/2), std::max(0, cy-lengthy/2),
    std::min(nx-1, cx+lengthx/2), std::min(ny-1, cy+lengthy/2),
    lx_backup, ly_backup);
}

constexpr double cdf97_a0 = -1.586134342;
constexpr double cdf97_a1 = -0.05298011854;
constexpr double cdf97_a2 = 0.8829110762;
constexpr double cdf97_a3 = 0.4435068522;
constexpr double cdf97_b  = 1.149604398;

template <typename T>
void
flift_cdf97_x(T* f, int nx, int ny, int nz, int lx, int ly, int lz)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mx <= 1) {
    return;
  }

  /* predict 1*/
  double a = cdf97_a0;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        T fleft  =          f[z*nx*ny+y*nx+(x-1)];
        T fright = x<mx-1 ? f[z*nx*ny+y*nx+(x+1)] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* update 1 */
  a = cdf97_a1;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        int xleft = x - 1;
        int xright = x<mx-1 ? x+1 : x-1;
        f[z*nx*ny+y*nx+xleft ] += a*f[z*nx*ny+y*nx+x];
        f[z*nx*ny+y*nx+xright] += a*f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* predict 2 */
  a = cdf97_a2;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        T fleft  =          f[z*nx*ny+y*nx+(x-1)];
        T fright = x<mx-1 ? f[z*nx*ny+y*nx+(x+1)] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* update 2 */
  a = cdf97_a3;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        int xleft = x - 1;
        int xright = x<mx-1 ? x+1 : x-1;
        f[z*nx*ny+y*nx+xleft ] += a*f[z*nx*ny+y*nx+x];
        f[z*nx*ny+y*nx+xright] += a*f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* scale */
  a = 1.0 / cdf97_b;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 0; x < mx; x += 2) {
        f[z*nx*ny+y*nx+x] /= a;
        if (x+1 < mx) {
          f[z*nx*ny+y*nx+(x+1)] *= a;
        }
      }
    }
  }

  /* pack */
  THREAD_LOCAL static std::vector<T> t; t.resize(mx>>1);
  int sx = (mx+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        t[x>>1]                = SCAST<T>(f[z*nx*ny+y*nx+ x   ]); // odd
        f[z*nx*ny+y*nx+(x>>1)] = SCAST<T>(f[z*nx*ny+y*nx+(x-1)]); // even
      }
      if ((mx&1) != 0) { // last even
        f[z*nx*ny+y*nx+(mx>>1)] = SCAST<T>(f[z*nx*ny+y*nx+(mx-1)]);
      }
      for (int x = 0; x < (mx>>1); ++x) {
        f[z*nx*ny+y*nx+(sx+x)] = t[x];
      }
    }
  }
}

template <typename T>
void
flift_cdf97_y(T* f, int nx, int ny, int nz, int lx, int ly, int lz)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (my <= 1) {
    return;
  }

  /* predict 1*/
  double a = cdf97_a0;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        T fleft  =          f[z*nx*ny+(y-1)*nx+x];
        T fright = y<my-1 ? f[z*nx*ny+(y+1)*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* update 1 */
  a = cdf97_a1;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        int yleft = y - 1;
        int yright = y<my-1 ? y+1 : y-1;
        f[z*nx*ny+yleft *nx+x] += a * f[z*nx*ny+y*nx+x];
        f[z*nx*ny+yright*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* predict 2 */
  a = cdf97_a2;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        T fleft  =          f[z*nx*ny+(y-1)*nx+x];
        T fright = y<my-1 ? f[z*nx*ny+(y+1)*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* update 2 */
  a = cdf97_a3;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        int yleft = y - 1;
        int yright = y<my-1 ? y+1 : y-1;
        f[z*nx*ny+yleft *nx+x] += a * f[z*nx*ny+y*nx+x];
        f[z*nx*ny+yright*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* scale */
  a = 1.0 / cdf97_b;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 0; y < my; y += 2) {
        f[z*nx*ny+y*nx+x] /= a;
        if (y+1 < my) {
          f[z*nx*ny+(y+1)*nx+x] *= a;
        }
      }
    }
  }

  /* pack */
  THREAD_LOCAL static std::vector<T> t; t.resize(my>>1);
  int sy = (my+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        t[y>>1]                = SCAST<T>(f[z*nx*ny+ y   *nx+x]); // odd
        f[z*nx*ny+(y>>1)*nx+x] = SCAST<T>(f[z*nx*ny+(y-1)*nx+x]); // even
      }
      if ((my&1) != 0) {
        f[z*nx*ny+(my>>1)*nx+x] = SCAST<T>(f[z*nx*ny+(my-1)*nx+x]);
      }
      for (int y = 0; y < (my>>1); ++y) {
        f[z*nx*ny+(sy+y)*nx+x] = t[y];
      }
    }
  }
}

template <typename T>
void
flift_cdf97_z(T* f, int nx, int ny, int nz, int lx, int ly, int lz)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mz <= 1) {
    return;
  }

  /* predict 1*/
  double a = cdf97_a0;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        T fleft  =          f[(z-1)*nx*ny+y*nx+x];
        T fright = z<mz-1 ? f[(z+1)*nx*ny+y*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* update 1 */
  a = cdf97_a1;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        int zleft  = z - 1;
        int zright = z<mz-1 ? z+1 : z-1;
        f[zleft *nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
        f[zright*nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* predict 2 */
  a = cdf97_a2;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        T fleft  =          f[(z-1)*nx*ny+y*nx+x];
        T fright = z<mz-1 ? f[(z+1)*nx*ny+y*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* update 2 */
  a = cdf97_a3;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        int zleft  = z - 1;
        int zright = z<mz-1 ? z+1 : z-1;
        f[zleft *nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
        f[zright*nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* scale */
  a = 1.0 / cdf97_b;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 0; z < mz; z += 2) {
        f[z*nx*ny+y*nx+x] /= a;
        if (z+1 < mz) {
          f[(z+1)*nx*ny+y*nx+x] *= a;
        }
      }
    }
  }

  /* pack */
  THREAD_LOCAL static std::vector<T> t; t.resize(mz>>1);
  int sz = (mz+1) >> 1;
  for (int y = 0; y < my; ++y) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        t[z>>1]                = SCAST<T>(f[ z   *nx*ny+y*nx+x]); // odd
        f[(z>>1)*nx*ny+y*nx+x] = SCAST<T>(f[(z-1)*nx*ny+y*nx+x]); // even
      }
      if ((mz&1) != 0) {
        f[(mz>>1)*nx*ny+y*nx+x] = SCAST<T>(f[(mz-1)*nx*ny+y*nx+x]);
      }
      for (int z = 0; z < (mz>>1); ++z) {
        f[(sz+z)*nx*ny+y*nx+x] = t[z];
      }
    }
  }
}

template <typename T>
void
ilift_cdf97_x(T* f, int nx, int ny, int nz, int lx, int ly, int lz)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mx <= 1) {
    return;
  }

  /* unpack */
  THREAD_LOCAL static std::vector<T> t; t.resize(mx>>1);
  int sx = (mx+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 0; x < (mx>>1); ++x) {
        t[x] = f[z*nx*ny+y*nx+(sx+x)];
      }
      for (int x = (mx>>1)*2-1; x >= 1; x -= 2) {
        f[z*nx*ny+y*nx+(x-1)] = SCAST<T>(f[z*nx*ny+y*nx+(x>>1)]); // even
        f[z*nx*ny+y*nx+ x   ] = SCAST<T>(t[x>>1]); // odd
      }
      if ((mx&1) != 0) { // last even
        f[z*nx*ny+y*nx+(mx-1)] = SCAST<T>(f[z*nx*ny+y*nx+(mx>>1)]);
      }
    }
  }

  /* scale */
  double a = cdf97_b;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 0; x < mx; x += 2) {
        f[z*nx*ny+y*nx+x] /= a;
        if (x+1 < mx) {
          f[z*nx*ny+y*nx+(x+1)] *= a;
        }
      }
    }
  }

  /* undo update 2 */
  a = -cdf97_a3;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        int xleft = x - 1;
        int xright = x<mx-1 ? x+1 : x-1;
        f[z*nx*ny+y*nx+xleft ] += a * f[z*nx*ny+y*nx+x];
        f[z*nx*ny+y*nx+xright] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* undo predict 2*/
  a = -cdf97_a2;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        T fleft  =          f[z*nx*ny+y*nx+(x-1)];
        T fright = x<mx-1 ? f[z*nx*ny+y*nx+(x+1)] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* undo update 1 */
  a = -cdf97_a1;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        int xleft = x - 1;
        int xright = x<mx-1 ? x+1 : x-1;
        f[z*nx*ny+y*nx+xleft ] += a * f[z*nx*ny+y*nx+x];
        f[z*nx*ny+y*nx+xright] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* undo predict 1 */
  a = -cdf97_a0;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int y = 0; y < my; ++y) {
      for (int x = 1; x < mx; x += 2) {
        T fleft  =          f[z*nx*ny+y*nx+(x-1)];
        T fright = x<mx-1 ? f[z*nx*ny+y*nx+(x+1)] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }
}

template <typename T>
void
ilift_cdf97_y(T* f, int nx, int ny, int nz, int lx, int ly, int lz)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (my <= 1) {
    return;
  }

  /* unpack */
  THREAD_LOCAL static std::vector<T> t; t.resize(my>>1);
  int sy = (my+1) >> 1;
  for (int z = 0; z < mz; ++z) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 0; y < (my>>1); ++y) {
        t[y] = f[z*nx*ny+(sy+y)*nx+x];
      }
      for (int y = (my>>1)*2-1; y >= 1; y -= 2) {
        f[z*nx*ny+(y-1)*nx+x] = SCAST<T>(f[z*nx*ny+(y>>1)*nx+x]); // even
        f[z*nx*ny+ y   *nx+x] = SCAST<T>(t[y>>1]); // odd
      }
      if ((my&1) != 0) { // last even
        f[z*nx*ny+(my-1)*nx+x] = SCAST<T>(f[z*nx*ny+(my>>1)*nx+x]);
      }
    }
  }

  /* scale */
  double a = cdf97_b;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 0; y < my; y += 2) {
        f[z*nx*ny+y*nx+x] /= a;
        if (y+1 < my) {
          f[z*nx*ny+(y+1)*nx+x] *= a;
        }
      }
    }
  }

  /* undo update 2 */
  a = -cdf97_a3;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        int yleft = y - 1;
        int yright = y<my-1 ? y+1 : y-1;
        f[z*nx*ny+yleft *nx+x] += a * f[z*nx*ny+y*nx+x];
        f[z*nx*ny+yright*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* undo predict 2*/
  a = -cdf97_a2;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        T fleft  =          f[z*nx*ny+(y-1)*nx+x];
        T fright = y<my-1 ? f[z*nx*ny+(y+1)*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* undo update 1 */
  a = -cdf97_a1;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        int yleft = y - 1;
        int yright = y<my-1 ? y+1 : y-1;
        f[z*nx*ny+yleft *nx+x] += a * f[z*nx*ny+y*nx+x];
        f[z*nx*ny+yright*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* undo predict 1 */
  a = -cdf97_a0;
  for (int z = 0; z < mz; ++z) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int y = 1; y < my; y += 2) {
        T fleft  =          f[z*nx*ny+(y-1)*nx+x];
        T fright = y<my-1 ? f[z*nx*ny+(y+1)*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }
}

template <typename T>
void
ilift_cdf97_z(T* f, int nx, int ny, int nz, int lx, int ly, int lz)
{
  int mx = nx>>lx, my = std::max(ny>>ly, 1), mz = std::max(nz>>lz, 1);
  if (mz <= 1) {
    return;
  }

  /* unpack */
  THREAD_LOCAL static std::vector<T> t; t.resize(mz>>1);
  for (int y = 0; y < my; ++y) {
    //PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      int sz = (mz+1) >> 1;
      for (int z = 0; z < (mz>>1); ++z) {
        t[z] = f[(sz+z)*nx*ny+y*nx+x];
      }
      for (int z = (mz>>1)*2-1; z >= 1; z -= 2) {
        f[(z-1)*nx*ny+y*nx+x] = SCAST<T>(f[(z>>1)*nx*ny+y*nx+x]); // even
        f[ z   *nx*ny+y*nx+x] = SCAST<T>(t[z>>1]); // odd
      }
      if ((mz&1) != 0) {
        f[(mz-1)*nx*ny+y*nx+x] = SCAST<T>(f[(mz>>1)*nx*ny+y*nx+x]);
      }
    }
  }

  /* scale */
  double a = cdf97_b;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 0; z < mz; z += 2) {
        f[z*nx*ny+y*nx+x] /= a;
        if (z+1 < mz) {
          f[(z+1)*nx*ny+y*nx+x] *= a;
        }
      }
    }
  }

  /* undo update 2 */
  a = -cdf97_a3;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        int zleft = z - 1;
        int zright = z<mz-1 ? z+1 : z-1;
        f[zleft *nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
        f[zright*nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* undo predict 2*/
  a = -cdf97_a2;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        T fleft  =          f[(z-1)*nx*ny+y*nx+x];
        T fright = z<mz-1 ? f[(z+1)*nx*ny+y*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }

  /* undo update 1 */
  a = -cdf97_a1;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        int zleft = z - 1;
        int zright = z<mz-1 ? z+1 : z-1;
        f[zleft *nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
        f[zright*nx*ny+y*nx+x] += a * f[z*nx*ny+y*nx+x];
      }
    }
  }

  /* Undo predict 1 */
  a = -cdf97_a0;
  for (int y = 0; y < my; ++y) {
    PARALLEL_LOOP
    for (int x = 0; x < mx; ++x) {
      for (int z = 1; z < mz; z += 2) {
        T fleft  =          f[(z-1)*nx*ny+y*nx+x];
        T fright = z<mz-1 ? f[(z+1)*nx*ny+y*nx+x] : fleft;
        f[z*nx*ny+y*nx+x] += a * (fleft+fright);
      }
    }
  }
}
