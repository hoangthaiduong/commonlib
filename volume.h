#pragma once

#include "my_math.h"
#include "vector.h"
#include <array>
#include <cstdint>
#include <vector>

/* Octree traversal order */
namespace {
std::array<uint8_t, 8> orders[4] = {
  { 127, 127, 127, 127, 127, 127, 127, 127 },  // not used
  {   0,   1, 127, 127, 127, 127, 127, 127 },  // for 1D
  {   0,   1,   2,   3, 127, 127, 127, 127 },  // for 2D
  {   0,   1,   2,   4,   3,   5,   6,   7 }}; // for 3D
}

FORCE_INLINE void
i2xyz(int n, int nx, int ny, int nz, OUT int* x, OUT int* y, OUT int* z)
{
  (void)nz;
  *z = n / (nx*ny);
  *x = n % (nx);
  *y = (n-*z*(nx*ny)) / nx;
}

FORCE_INLINE int
xyz2i(int nx, int ny, int nz, int x, int y, int z)
{
  (void)nz;
  return z*nx*ny + y*nx + x;
}

struct Set {
  int xyz;
  int mxyz;
};

/* Coarsest subband is at the end */
inline void
build_subbands(int D, int nx, int ny, int nz, int nlevels, OUT std::vector<Set>* I)
{
  REQUIRE(nz==1 || D==3);
  REQUIRE(ny==1 || D>=2);
  const std::array<uint8_t, 8>& order = orders[D];
  I->reserve(((1<<D)-1)*nlevels+1);
  int mx = nx, my = ny, mz = nz;
  for (int i = 0; i < nlevels; ++i) {
    int px = (mx+1)>>1, py = (my+1)>>1, pz = (mz+1)>>1;
    for (int j = (1<<D)-1; j > 0; --j) {
      uint8_t x = order[j]&1u, y = (order[j]>>1)&1u, z = (order[j]>>2)&1u;
      int smx = (x==0)?px:mx-px, smy = (y==0)?py:my-py, smz = (z==0)?pz:mz-pz;
      Set sb = {
        xyz2i(nx, ny, nz, x*px, y*py, z*pz),
        xyz2i(nx, ny, nz, smx, smy, smz) };
      if (smx*smy*smz != 0) { // child exists
        I->emplace_back(sb);
      }
    }
    mx = px; my = py; mz = pz;
  }
  I->emplace_back(Set{ 0, xyz2i(nx, ny, nz, mx, my, mz) });
}

// TODO: constraint D to 2 and 3
template <typename T>
void
blocks_to_subbands(int D, const T* f_in, int nx, int ny, int nz, int N, OUT T* f_out)
{
  // TODO: simplify the prerequisites
  REQUIRE(nx%N==0 && ny%N==0);
  if (D == 3) {
    REQUIRE(nz%N == 0);
  }
  else {
    REQUIRE(nz == 1);
  }
  std::vector<Set> I;
  int nlevels = logi2(N);
  int Nx = N, Ny = N, Nz = (D==3)?N:1;
  build_subbands(D, Nx, Ny, Nz, nlevels, &I);
  int size = CINT(I.size());
  int mx = nx>>nlevels, my = ny>>nlevels, mz = nz>>nlevels;
  for (int i = size-1; i >= 0; --i) {
    const Set& s = I[i];
    int sx, sy, sz; i2xyz(s.xyz, Nx, Ny, Nz, &sx, &sy, &sz);
    int smx, smy, smz; i2xyz(s.mxyz, Nx, Ny, Nz, &smx, &smy, &smz);
    for (int z = 0; z < nz; z += N) {
      int iz = z + sz;
      int oz = sz*mz + (z>>nlevels)*smz;
      for (int y = 0; y < ny; y += N) {
        int iy = y + sy;
        int oy = sy*my + (y>>nlevels)*smy;
        for (int x = 0; x < nx; x += N) {
          int ix = x + sx;
          int ox = sx*mx + (x>>nlevels)*smx;
          for (int zz = 0; zz < smz; ++zz) {
            for (int yy = 0; yy < smy; ++yy) {
              for (int xx = 0; xx < smx; ++xx) {
                f_out[(oz+zz)*nx*ny+(oy+yy)*nx+(ox+xx)] = f_in[(iz+zz)*nx*ny+(iy+yy)*nx+(ix+xx)];
              }
            }
          }
        }
      }
    }
  }
}

template <typename T>
void
subbands_to_blocks(int D, const T* f_in, int nx, int ny, int nz, int N, OUT T* f_out)
{
  // TODO: simplify the prerequisites
  REQUIRE(nx%N==0 && ny%N==0);
  if (D == 3) {
    REQUIRE(nz%N == 0);
  }
  else {
    REQUIRE(nz == 1);
  }
  std::vector<Set> I;
  int nlevels = logi2(N);
  int Nx = N, Ny = N, Nz = (D==3)?N:1;
  build_subbands(D, Nx, Ny, Nz, nlevels, &I);
  int size = CINT(I.size());
  int mx = nx>>nlevels, my = ny>>nlevels, mz = nz>>nlevels;
  for (int i = size-1; i >= 0; --i) {
    const Set& s = I[i];
    int sx, sy, sz; i2xyz(s.xyz, Nx, Ny, Nz, &sx, &sy, &sz);
    int smx, smy, smz; i2xyz(s.mxyz, Nx, Ny, Nz, &smx, &smy, &smz);
    for (int z = 0; z < nz; z += N) {
      int iz = z + sz;
      int oz = sz*mz + (z>>nlevels)*smz;
      for (int y = 0; y < ny; y += N) {
        int iy = y + sy;
        int oy = sy*my + (y>>nlevels)*smy;
        for (int x = 0; x < nx; x += N) {
          int ix = x + sx;
          int ox = sx*mx + (x>>nlevels)*smx;
          for (int zz = 0; zz < smz; ++zz) {
            for (int yy = 0; yy < smy; ++yy) {
              for (int xx = 0; xx < smx; ++xx) {
                f_out[(iz+zz)*nx*ny+(iy+yy)*nx+(ix+xx)] = f_in[(oz+zz)*nx*ny+(oy+yy)*nx+(ox+xx)];
              }
            }
          }
        }
      }
    }
  }
}

/* Take a z slice from a volume */
template <typename T>
void
slice_z(const T* RESTRICT f, int nx, int ny, int nz, int z, OUT T* RESTRICT s)
{
  PARALLEL_LOOP
  for (int y = 0; y < ny; ++y) {
    for (int x = 0; x < nx; ++x) {
      int i = z*nx*ny + y*nx + x;
      int j = y*nx + x;
      s[j] = f[i];
    }
  }
}

/* Compute discrete Laplacian on f using three-point stencil */
template <typename T>
void
laplacian(const T* RESTRICT f, int nx, int ny, int nz, OUT T* RESTRICT g)
{
  /* along x */
  if (nx >= 3) {
    for (int z = 0; z < nz; ++z) {
      PARALLEL_LOOP
      for (int y = 0; y < ny; ++y) {
        g[z*nx*ny+y*nx+0] = f[z*nx*ny+y*nx+0] - 2*f[z*nx*ny+y*nx+1] + f[z*nx*ny+y*nx+2];
        for (int x = 1; x < nx-1; ++x) {
          g[z*nx*ny+y*nx+x] = f[z*nx*ny+y*nx+(x-1)] - 2*f[z*nx*ny+y*nx+x] + f[z*nx*ny+y*nx+(x+1)];
        }
        g[z*nx*ny+y*nx+(nx-1)] = f[z*nx*ny+y*nx+(nx-1)] - 2*f[z*nx*ny+y*nx+(nx-2)] + f[z*nx*ny+y*nx+(nx-3)];
      }
    }
  }
  /* along y */
  if (ny >= 3) {
    for (int z = 0; z < nz; ++z) {
      PARALLEL_LOOP
      for (int x = 0; x < nx; ++x) {
        g[z*nx*ny+0*nx+x] += f[z*nx*ny+0*nx+x] - 2*f[z*nx*ny+1*nx+x] + f[z*nx*ny+2*nx+x];
        for (int y = 1; y < ny-1; ++y) {
          g[z*nx*ny+y*nx+x] += f[z*nx*ny+(y-1)*nx+x] - 2*f[z*nx*ny+y*nx+x] + f[z*nx*ny+(y+1)*nx+x];
        }
        g[z*nx*ny+(ny-1)*nx+x] += f[z*nx*ny+(ny-1)*nx+x] - 2*f[z*nx*ny+(ny-2)*nx+x] + f[z*nx*ny+(ny-3)*nx+x];
      }
    }
  }
  /* along z */
  if (nz >= 3) {
    for (int y = 0; y < ny; ++y) {
      PARALLEL_LOOP
      for (int x = 0; x < nx; ++x) {
        g[0*nx*ny+y*nx+x] += f[0*nx*ny+y*nx+x] - 2*f[1*nx*ny+y*nx+x] + f[2*nx*ny+y*nx+x];
        for (int z = 1; z < nz-1; ++z) {
          g[z*nx*ny+y*nx+x] += f[(z-1)*nx*ny+y*nx+x] - 2*f[z*nx*ny+y*nx+x] + f[(z+1)*nx*ny+y*nx+x];
        }
        g[(nz-1)*nx*ny+y*nx+x] += f[(nz-1)*nx*ny+y*nx+x] - 2*f[(nz-2)*nx*ny+y*nx+x] + f[(nz-3)*nx*ny+y*nx+x];
      }
    }
  }
}

/* Compute discrete derivative using three-point stencil */
template <typename T>
void
gradient(
  const T* RESTRICT f, int nx, int ny, int nz,
  OUT T* RESTRICT gx, OUT T* RESTRICT gy, OUT T* RESTRICT gz)
{
  /* along x */
  if (nx >= 3) {
    for (int z = 0; z < nz; ++z) {
      PARALLEL_LOOP
      for (int y = 0; y < ny; ++y) {
        gx[z*nx*ny+y*nx+0] = f[z*nx*ny+y*nx+1] - f[z*nx*ny+y*nx+0];
        for (int x = 1; x+1 < nx; ++x) {
          gx[z*nx*ny+y*nx+x] = (f[z*nx*ny+y*nx+(x+1)]-f[z*nx*ny+y*nx+(x-1)]) * 0.5;
        }
        gx[z*nx*ny+y*nx+(nx-1)] = f[z*nx*ny+y*nx+(nx-1)] - f[z*nx*ny+y*nx+(nx-2)];
      }
    }
  }
  /* along y */
  if (ny >= 3) {
    for (int z = 0; z < nz; ++z) {
      PARALLEL_LOOP
      for (int x = 0; x < nx; ++x) {
        gy[z*nx*ny+0*nx+x] = f[z*nx*ny+1*nx+x] - f[z*nx*ny+0*nx+x];
        for (int y = 1; y+1 < ny; ++y) {
          gy[z*nx*ny+y*nx+x] = (f[z*nx*ny+(y+1)*nx+x]-f[z*nx*ny+(y-1)*nx+x]) * 0.5;
        }
        gy[z*nx*ny+(ny-1)*nx+x] = f[z*nx*ny+(ny-1)*nx+x] - f[z*nx*ny+(ny-2)*nx+x];
      }
    }
  }
  /* along z */
  if (nz >= 3) {
    for (int y = 0; y < ny; ++y) {
      PARALLEL_LOOP
      for (int x = 0; x < nx; ++x) {
        gz[0*nx*ny+y*nx+x] = f[1*nx*ny+y*nx+x] - f[0*nx*ny+y*nx+x];
        for (int z = 1; z+1 < nz; ++z) {
          gz[z*nx*ny+y*nx+x] = (f[(z+1)*nx*ny+y*nx+x]-f[(z-1)*nx*ny+y*nx+x]) * 0.5;
        }
        gz[(nz-1)*nx*ny+y*nx+x] = f[(nz-1)*nx*ny+y*nx+x] - f[(nz-2)*nx*ny+y*nx+x];
      }
    }
  }
}

/* Compute the norm of the difference between two vector functions */
template <typename T>
double
norm_diff(const T* fx, const T* fy, const T* gx, const T* gy, int64_t n)
{
  double error = 0;
  for (int64_t i = 0; i < n; ++i) {
    double diff_x = fx[i] - gx[i];
    double diff_y = fy[i] - gy[i];
    error += diff_x*diff_x + diff_y*diff_y;
  }
  error /= n;
  return sqrt(error);
}

template <typename T>
float
marching_squares(
  const T* f, int nx, int ny, float val, bool compute_area, OUT std::vector<Point2f>* segments)
{
  const Point2f coords[] = {
    Point2f{0, 0},
    Point2f{1, 0},
    Point2f{1, 1},
    Point2f{0, 1},
    Point2f{0, 0}
  };
  segments->reserve(128);
  float area = 0;
  Point2f polygon[2][13];
  for (int y = 0; y+1 < ny; ++y) {
    for (int x = 0; x+1 < nx; ++x) {
      T f_cell[] = {
        f[(y+0)*nx+(x+0)],
        f[(y+0)*nx+(x+1)],
        f[(y+1)*nx+(x+1)],
        f[(y+1)*nx+(x+0)],
        f[(y+0)*nx+(x+0)] };
      int poly_vert_idx = 0;
      /* extract intersections */
      for (int i = 0; i < 4; ++i) {
        Point2f p0 = coords[i+0] + Point2f(float(x), float(y));
        Point2f p1 = coords[i+1] + Point2f(float(x), float(y));
        T f0 = f_cell[i+0];
        T f1 = f_cell[i+1];
        Point2f intersection = p0 + (p1-p0)*(val-f_cell[i])/(f_cell[i+1]-f_cell[i]);
        int first = f0<=val, second = (val-f0)*(val-f1)<=0, third = f1<=val;
        REQUIRE(poly_vert_idx < 12);
        polygon[first][poly_vert_idx] = p0; poly_vert_idx += first;
        REQUIRE(poly_vert_idx < 12);
        polygon[second][poly_vert_idx] = intersection; poly_vert_idx += second;
        REQUIRE(poly_vert_idx < 12);
        polygon[third][poly_vert_idx] = p1; poly_vert_idx += third;
      }
      polygon[1][poly_vert_idx] = polygon[1][0];
      if (compute_area) {
        area += polygon_area(polygon[1], poly_vert_idx+1);
      }
      /* remove duplicated points */
      int j = 0;
      for (int i = 1; i < poly_vert_idx; i += 2) {
        REQUIRE(i<12);
        Point2f curr = polygon[1][i+0];
        REQUIRE(i<12);
        Point2f next = polygon[1][i+1];
        int duplicate = curr == next;
        REQUIRE(j < 12);
        polygon[duplicate][j] = curr; j += !duplicate;
        REQUIRE(j < 12);
        polygon[duplicate][j] = next; j += !duplicate;
      }
      for (int i = 0; i < j; ++i) {
        segments->push_back(polygon[0][i]);
      }
    }
  }

  return area;
}
