#pragma once

#include <cstdint>
#include <limits>
#include <type_traits>

/* Syntactic sugars for enable_if */
template <typename T>
using enable_if_int = typename std::enable_if_t<std::is_integral<T>::value>;
template <typename T>
using enable_if_float = typename std::enable_if_t<std::is_floating_point<T>::value>;
template <typename T>
using enable_if_unsigned = typename std::enable_if_t<std::is_unsigned<T>::value>;
template <typename T>
using enable_if_signed = typename std::enable_if_t<std::is_signed<T>::value>;
template <int N>
using enable_if_pow2 = typename std::enable_if_t<(N&(N-1))==0>;

/* Integer traits */
template <typename T,typename = enable_if_int<T>>
struct IntTraits {};

template <>
struct IntTraits<uint8_t> {
  static constexpr uint8_t nbmask = 0xaa; // nega-binary mask
  static constexpr uint8_t min = std::numeric_limits<uint8_t>::min();
  static constexpr uint8_t max = std::numeric_limits<uint8_t>::max();
};
template <>
struct IntTraits<uint16_t> {
  static constexpr uint16_t nbmask = 0xaaaa; // nega-binary mask
  static constexpr uint16_t min = std::numeric_limits<uint16_t>::min();
  static constexpr uint16_t max = std::numeric_limits<uint16_t>::max();
};
template <>
struct IntTraits<uint32_t> {
  static constexpr uint32_t nbmask = 0xaaaaaaaa; // nega-binary mask
  static constexpr uint32_t min = std::numeric_limits<uint32_t>::min();
  static constexpr uint32_t max = std::numeric_limits<uint32_t>::max();
};
template <>
struct IntTraits<uint64_t> {
  static constexpr uint64_t nbmask = 0xaaaaaaaaaaaaaaaa; // nega-binary mask
  static constexpr uint64_t min = std::numeric_limits<uint64_t>::min();
  static constexpr uint64_t max = std::numeric_limits<uint64_t>::max();
};

/* Floating point traits */
template <typename T,typename = enable_if_float<T>>
struct FloatTraits {};

template <>
struct FloatTraits<float> {
  static constexpr int ebits = 8; // number of exponent bits
  static constexpr int ebias = (1<<(ebits-1)) - 1; // exponent bias
  static constexpr int mantissa = 23;
  static constexpr float min = -std::numeric_limits<float>::max();
  static constexpr float max = std::numeric_limits<float>::max();
  using Int = int32_t;
  using UInt = uint32_t;
};
template <>
struct FloatTraits<double> {
  static constexpr int ebits = 11; // number of exponent bits
  static constexpr int ebias = (1<<(ebits-1)) - 1; // exponent bias
  static constexpr int mantissa = 52;
  static constexpr double min = -std::numeric_limits<double>::max();
  static constexpr double max = std::numeric_limits<double>::max();
  using Int = int64_t;
  using UInt = uint64_t;
};
