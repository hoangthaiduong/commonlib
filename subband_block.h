#pragma once

#include "macros.h"
#include "volume.h"
#include <fstream>
#include <iomanip>
#include <vector>

struct Params {
  std::string data_file;
  std::string output_file;
  int nx = 0, ny = 0, nz = 0;
  std::string dtype; // options: float32, float64
  std::string quantity; // options: rms, gradient, laplacian, histogram
  std::string transform; // options: cdf53, cdf97
  int nlevels = 0;
  int bits_quantize = 0;
  int block_size = 0;
  bool negabinary = 0;
  float isoval = 0;
};
static const int nbins_ = 512;

struct SubbandBlock
{
  int xyz;
  int mxyz;
  double score;
  int8_t bit;
};

inline void
read_subband_blocks(const char* file_name, OUT Params* p, OUT std::vector<SubbandBlock>* blocks)
{
  std::ifstream in_stream = std::ifstream(file_name);
  int version;
  in_stream >> version;
  in_stream >> p->data_file;
  in_stream >>
    p->nx >> p->ny >> p->nz >> p->transform >> p->nlevels >>
    p->bits_quantize;
  if (version >= 1) {
    in_stream >> p->isoval;
  }
  in_stream >> p->block_size;
  int nblocks; in_stream >> nblocks;
  blocks->resize(nblocks);
  for (SubbandBlock& sbb : *blocks) {
    int sx, sy, sz; in_stream >> sx >> sy >> sz;
    sbb.xyz = xyz2i(p->nx, p->ny, p->nz, sx, sy, sz);
    int mx, my, mz; in_stream >> mx >> my >> mz;
    sbb.mxyz = xyz2i(p->nx, p->ny, p->nz, mx, my, mz);
    int bit;
    in_stream >> bit >> sbb.score; sbb.bit = CINT8(bit);
  }
}

inline void
write_subband_blocks(int version, const Params& p, const std::vector<SubbandBlock>& blocks)
{
  /* serialize the output to a file */
  std::ofstream out_stream = std::ofstream(p.output_file.data());
  out_stream << version << "\n";
  out_stream << std::setprecision(17);
  out_stream << p.data_file << "\n";
  out_stream << p.nx << " " << p.ny << " " << p.nz << "\n";
  out_stream << p.transform << "\n";
  out_stream << p.nlevels << "\n";
  out_stream << p.bits_quantize << "\n";
  if (version >= 1) {
    out_stream << p.isoval << "\n";
  }
  out_stream << p.block_size << "\n";
  out_stream << blocks.size() << "\n";
  for (const SubbandBlock& sbb : blocks) {
    int sx, sy, sz; i2xyz(sbb.xyz, p.nx, p.ny, p.nz, &sx, &sy, &sz);
    int mx, my, mz; i2xyz(sbb.mxyz, p.nx, p.ny, p.nz, &mx, &my, &mz);
    out_stream <<
      sx << " " << sy << " " << sz << "        " <<
      mx << " " << my << " " << mz << "        "  <<
      int(sbb.bit) << "         " << sbb.score << "\n";
  }
}
