#pragma once

template <typename T>
struct Vec3
{
  T x = static_cast<T>(-1);
  T y = static_cast<T>(-1);
  T z = static_cast<T>(-1);
  Vec3() = default;
  Vec3(T x_, T y_, T z_) : x(x_), y(y_), z(z_) {}
  Vec3(T a) : x(a), y(a), z(a) {}

  /* Type-converting constructor */
  template <typename U>
  Vec3(Vec3<U> other) : x(other.x), y(other.y), z(other.z) {}

  T product() const { return x * y * z; }
};

/* Print a Vec3 to an output stream */
template <typename T>
std::ostream&
operator<<(std::ostream& os, Vec3<T> p)
{
  os << "(" << p.x << ", " << p.y << ", " << p.z << ")";
  return os;
}

/* Compare two vectors component-wise */
template <typename T>
bool
operator==(Vec3<T> a, Vec3<T> b)
{
  return a.x==b.x && a.y==b.y && a.z==b.z;
}

/* Compare all three components to a scalar for equality */
template <typename T, typename U>
bool
operator==(Vec3<T> a, U s)
{
  return a.x==s && a.y==s && a.z==s;
}

/* Compare all three components to a scalar for greater than */
template <typename T, typename U>
bool
operator>(Vec3<T> a, U s)
{
  return a.x>s && a.y>s && a.z>s;
}

/* Compare two vectors for greater than component-wise */
template <typename T, typename U>
bool
operator>(Vec3<T> a, Vec3<U> b)
{
  return a.x>b.x && a.y>b.y && a.z>b.z;
}

/* Compare all three components to a scalar for greater than or equal to */
template <typename T, typename U>
bool
operator>=(Vec3<T> a, U s)
{
  return a.x>=s && a.y>=s && a.z>=s;
}

/* Compare two vectors for less than component-wise */
template <typename T, typename U>
bool
operator<(Vec3<T> a, Vec3<U> b)
{
  return a.x<b.x && a.y<b.y && a.z<b.z;
}

/* Add a scalar to each component */
template <typename T, typename U>
Vec3<T>
operator+(Vec3<T> a, U s)
{
  return Vec3<T>(a.x+s, a.y+s, a.z+s);
}

/* Add two vectors component-wise */
template <typename T>
Vec3<T>
operator+(Vec3<T> a, Vec3<T> b)
{
  return Vec3<T>(a.x+b.x, a.y+b.y, a.z+b.z);
}

/* Subtract a scalar from each component */
template <typename T, typename U>
Vec3<T>
operator-(Vec3<T> a, U s)
{
  return Vec3<T>(a.x-s, a.y-s, a.z-s);
}

/* Subtract a vector from another component-wise */
template <typename T>
Vec3<T>
operator-(Vec3<T> a, Vec3<T> b)
{
  return Vec3<T>(a.x-b.x, a.y-b.y, a.z-b.z);
}

/* Multiply two vectors component-wise */
template <typename T>
Vec3<T>
operator*(Vec3<T> a, Vec3<T> b)
{
  return Vec3<T>(a.x*b.x, a.y*b.y, a.z*b.z);
}

/* Multiply a scalar to each component */
template <typename T, typename U>
Vec3<T>
operator*(Vec3<T> a, U s)
{
  return Vec3<T>(a.x*s, a.y*s, a.z*s);
}

/* Divide a vector from another component-wise */
template <typename T>
Vec3<T>
operator/(Vec3<T> a, Vec3<T> b)
{
  T x = b.x!=0 ? a.x/b.x : 0;
  T y = b.y!=0 ? a.y/b.y : 0;
  T z = b.z!=0 ? a.z/b.z : 0;
  return Vec3<T>(x, y, z);
}

/* Divide each component by a scalar */
template <typename T, typename U>
Vec3<T>
operator/(Vec3<T> a, U s)
{
  return s==0 ? Vec3<T>(0, 0, 0) : Vec3<T>(a.x/s, a.y/s, a.z/s);
}

/* Take component-wise remainder */
template <typename T>
Vec3<T>
operator%(Vec3<T> a, Vec3<T> b)
{
  return Vec3<T>(a.x%b.x, a.y%b.y, a.z%b.z);
}

/* ---------------------------------------------------------------- */

template <typename T>
struct Vec2
{
  T x = static_cast<T>(-1);
  T y = static_cast<T>(-1);
  Vec2() = default;
  Vec2(T x_, T y_) : x(x_), y(y_) {}
  Vec2(T a) : x(a), y(a) {}

  /* Type-converting constructor */
  template <typename U>
  Vec2(Vec2<U> other) : x(other.x), y(other.y) {}

  T product() const { return x * y; }
  double length() const { return sqrt(x*x+y*y); }
};

/* Print a Vec2 to an output stream */
template <typename T>
std::ostream&
operator<<(std::ostream& os, Vec2<T> p)
{
  os << "(" << p.x << ", " << p.y << ")";
  return os;
}

/* Compare two vectors component-wise */
template <typename T>
bool
operator==(Vec2<T> a, Vec2<T> b)
{
  return a.x==b.x && a.y==b.y;
}

/* Compare all three components to a scalar for equality */
template <typename T, typename U>
bool
operator==(Vec2<T> a, U s)
{
  return a.x==s && a.y==s;
}

/* Compare all three components to a scalar for greater than */
template <typename T, typename U>
bool
operator>(Vec2<T> a, U s)
{
  return a.x>s && a.y>s;
}

/* Compare two vectors for greater than component-wise */
template <typename T, typename U>
bool
operator>(Vec2<T> a, Vec2<U> b)
{
  return a.x>b.x && a.y>b.y;
}

/* Compare all three components to a scalar for greater than or equal to */
template <typename T, typename U>
bool
operator>=(Vec2<T> a, U s)
{
  return a.x>=s && a.y>=s;
}

/* Compare two vectors for less than component-wise */
template <typename T, typename U>
bool
operator<(Vec2<T> a, Vec2<U> b)
{
  return a.x<b.x && a.y<b.y;
}

/* Add a scalar to each component */
template <typename T, typename U>
Vec2<T>
operator+(Vec2<T> a, U s)
{
  return Vec2<T>(a.x+s, a.y+s);
}

/* Add two vectors component-wise */
template <typename T>
Vec2<T>
operator+(Vec2<T> a, Vec2<T> b)
{
  return Vec2<T>(a.x+b.x, a.y+b.y);
}

/* Subtract a scalar from each component */
template <typename T, typename U>
Vec2<T>
operator-(Vec2<T> a, U s)
{
  return Vec2<T>(a.x-s, a.y-s);
}

/* Subtract a vector from another component-wise */
template <typename T>
Vec2<T>
operator-(Vec2<T> a, Vec2<T> b)
{
  return Vec2<T>(a.x-b.x, a.y-b.y);
}

/* Multiply two vectors component-wise */
template <typename T>
Vec2<T>
operator*(Vec2<T> a, Vec2<T> b)
{
  return Vec2<T>(a.x*b.x, a.y*b.y);
}

/* Multiply a scalar to each component */
template <typename T, typename U>
Vec2<T>
operator*(Vec2<T> a, U s)
{
  return Vec2<T>(a.x*s, a.y*s);
}

/* Divide a vector from another component-wise */
template <typename T>
Vec2<T>
operator/(Vec2<T> a, Vec2<T> b)
{
  T x = b.x!=0 ? a.x/b.x : 0;
  T y = b.y!=0 ? a.y/b.y : 0;
  return Vec2<T>(x, y);
}

/* Divide each component by a scalar */
template <typename T, typename U>
Vec2<T>
operator/(Vec2<T> a, U s)
{
  return s==0 ? Vec2<T>(0, 0) : Vec2<T>(a.x/s, a.y/s);
}

/* Take component-wise remainder */
template <typename T>
Vec2<T>
operator%(Vec2<T> a, Vec2<T> b)
{
  return Vec2<T>(a.x%b.x, a.y%b.y);
}

template <typename T>
T
distance(Vec2<T> a, Vec2<T> b)
{
  return (a-b).length();
}

template <typename T>
using Point2 = Vec2<T>;
using Point2i = Point2<int>;
using Point2d = Point2<double>;
using Point2f = Point2<float>;
