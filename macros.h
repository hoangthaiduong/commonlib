#pragma once

/* Annotations for function output parameters */
#define OUT
#define IN_OUT

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

/* Assertion */
#ifdef ENABLE_ASSERT
#include <cstdio>
#include <exception>
#define PRINT_ERROR(format, ...) \
  fprintf(stderr, "Error in function %s, file %s, line %d\n", \
          __FUNCTION__, __FILE__, __LINE__); \
  fprintf(stderr, format, __VA_ARGS__);
#define REQUIRE_MSG(condition, format, ...) \
  if (!(condition)) { \
    PRINT_ERROR(format, __VA_ARGS__) \
    throw std::exception(); \
  }
#define REQUIRE(condition) \
  if (!(condition)) { \
    throw std::exception(); \
  }
#else
#define REQUIRE_MSG(condition, message)
#define REQUIRE(condition)
#endif // ENABLE_ASSERT

/* Force inlining */
#if defined(_MSC_VER)
#define FORCE_INLINE __forceinline
#else
#define FORCE_INLINE __attribute__((always_inline)) inline
#endif

/* restrict */
#if defined(_MSC_VER)
#define RESTRICT __restrict
#else
#define RESTRICT restrict
#endif

/* Thread local hack for clang */
#if defined(__clang__)
#define THREAD_LOCAL
#else
#define THREAD_LOCAL thread_local
#endif

/* Pragma parallel */
#if defined(_MSC_VER)
#define PARALLEL_LOOP \
  __pragma(loop(hint_parallel(8))) \
  __pragma(loop(ivdep))
#else
// TODO: define OpenMP loop
#define PARALLEL_LOOP(n)
#endif

/* Types */
#include <string>
#include <vector>
#include <cstdint>
using uint = unsigned int;
using VecInt = std::vector<int>;
using VecUInt8 = std::vector<uint8_t>;
using VecInt8 = std::vector<int8_t>;
using VecUInt64 = std::vector<uint64_t>;
using VecInt64 = std::vector<int64_t>;
using VecDouble = std::vector<double>;
using VecFloat = std::vector<float>;

enum struct Types : int8_t {
  Int8    = 1, UInt8   = 1,
  Int16   = 2, UInt16  = 2,
  Int32   = 4, UInt32  = 4,
  Int64   = 8, UInt64  = 8,
  Float32 = 4, Float64 = 8
};

/* Type casts */
#define SCAST static_cast
#define RCAST reinterpret_cast
#define CINT(x) static_cast<int>(x)
#define CUINT(x) static_cast<unsigned int>(x)
#define CINT8(x) static_cast<int8_t>(x)
#define CINT64(x) static_cast<int64_t>(x)
#define CUINT64(x) static_cast<uint64_t>(x)
#define CUINT8(x) static_cast<uint8_t>(x)
#define CDOUBLE(x) static_cast<double>(x)
#define CSIZE_T(x) static_cast<size_t>(x)
#define CSTRING(x) std::to_string(x)

#include <climits>
#define BIT_SIZE(x) (CHAR_BIT * sizeof(x))
