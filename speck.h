#pragma once

#include "bit_ops.h"
#include "bitstream.h"
#include "macros.h"
#include "template.h"
#include "volume.h"

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <vector>

struct Node {
  int xyz;
  int mxyz;
  int children[8]; // indices of the children nodes
  int8_t b; // first significant bit plane
};

int indent = 0;

//#define SPECK_DEBUG

/* Build an octree from a subband, where each node in the octree stores the first
significant bit plane of any cells under the node. */
template <typename T, typename = enable_if_unsigned<T>>
int8_t
build_octree(
  int D, const T* f, int nx, int ny, int nz,
  IN_OUT int* node_idx, IN_OUT std::vector<Node>* octree, OUT VecInt8* leaves)
{
  REQUIRE(D <= 3);
  REQUIRE(!octree->empty());
  REQUIRE(leaves->size() == nx*ny*nz);

  int s_idx = *node_idx;
  Node* node = &(*octree)[s_idx];
  int mx, my, mz; i2xyz(node->mxyz, nx, ny, nz, &mx, &my, &mz);
  int sx, sy, sz; i2xyz(node->xyz, nx, ny, nz, &sx, &sy, &sz);
  int px = (mx+1)>>1, py = (my+1)>>1, pz = (mz+1)>>1;
  int8_t cb[8];
  int nchildren = 0;
  const std::array<uint8_t, 8>& order = orders[D];
  for (int i = 0; i < (1<<D); ++i) {
    uint8_t x = order[i]&1u, y = (order[i]>>1)&1u, z = (order[i]>>2)&1u;
    int cx = sx+x*px , cy = sy+y*py, cz = sz+z*pz;
    int cmx = (x==0)?px:mx-px, cmy = (y==0)?py:my-py, cmz = (z==0)?pz:mz-pz;
    Node c = {
      xyz2i(nx, ny, nz, cx, cy, cz),
      xyz2i(nx, ny, nz, cmx, cmy, cmz), {}, -1 };
    if (cmx*cmy*cmz != 0) { // child exists
      if (cmx*cmy*cmz == 1) { // single point
        int8_t b = bsr(f[xyz2i(nx, ny, nz, cx, cy, cz)]);
        (*leaves)[c.xyz]= b;
        node->children[nchildren] = c.xyz;
        cb[nchildren] = b;
      }
      else {
        octree->emplace_back(c);
        node = &(*octree)[s_idx];
        ++(*node_idx);
        node->children[nchildren] = *node_idx;
        cb[nchildren] = build_octree<T>(D, f, nx, ny, nz, node_idx, octree, leaves);
        node = &(*octree)[s_idx];
      }
      ++nchildren;
    }
  }
  /* combine the children's first significant bit planes */
  node->b = -1;
  for (int i = 0; i < nchildren; ++i) {
    node->b = std::max(node->b, cb[i]);
  }

  return node->b;
}

void
code_s(
  int D, int nx, int ny, int nz, int b, int s_index,
  const std::vector<Node>& octree, const VecInt8& leaves,
  IN_OUT VecInt* lis, IN_OUT VecInt* lip, OUT VecInt* lsp, OUT BitStream<>* stream)
{
  indent += 2;
#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Code S " << "\n";
#endif
  const std::array<uint8_t, 8>& order = orders[D];
  const Node& s = octree[s_index];
  int sx, sy, sz;
  i2xyz(s.xyz, nx, ny, nz, &sx, &sy, &sz);
  int smx, smy, smz;
  i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
  int px = (smx+1)>>1, py = (smy+1)>>1, pz = (smz+1)>>1;
  int nchildren = 0;
  for (int i = 0; i < (1<<D); ++i) {
    uint8_t x = order[i]&1u, y = (order[i]>>1)&1u, z = (order[i]>>2)&1u;
    int cx = sx+x*px, cy = sy+y*py, cz = sz+z*pz;
    int cmx = (x==0)?px:smx-px, cmy = (y==0)?py:smy-py, cmz = (z==0)?pz:smz-pz;
    if (cmx*cmy*cmz != 0) { // child exists
      int c_idx = s.children[nchildren++];
      bool single_point = cmx*cmy*cmz==1;
      int8_t cb = single_point ? leaves[c_idx] : octree[c_idx].b;
      stream->write_bit(cb >= b);
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Write bit " << (cb >= b) << std::endl;
#endif
      // TODO: try switching loop order to see if performance improves
      if (cb >= b) { // significant
        if (single_point) { // single point
          lsp->emplace_back(xyz2i(nx, ny, nz, cx, cy, cz));
#ifdef SPECK_DEBUG
          std::cout <<  std::string(indent, ' ') << "  Add to LSP " << cx << " " << cy << " " << cz << std::endl;
#endif
        }
        else { // not single point
          code_s(D, nx, ny, nz, b, c_idx, octree, leaves, lis, lip, lsp, stream);
        }
      }
      else { // insignificant
        if (single_point) {
          lip->emplace_back(c_idx);
#ifdef SPECK_DEBUG
          std::cout <<  std::string(indent, ' ') << "  Add to LIP " << cx << " "<< cy << " " << cz << std::endl;
#endif
        }
        else {
          lis->emplace_back(c_idx);
#ifdef SPECK_DEBUG
          std::cout <<  std::string(indent, ' ') << "  Add to LIS " << cx << " "<< cy << " " << cz << std::endl;
#endif
        }
      }
    }
  }
  indent -= 2;
}

enum class From { Octree, Lis, Lip };

/* The LIS stores a list of pairs where each pair contains an index to the octree list and
an index to the list of octree nodes.
If o_index < 0 (the set S is in LIS), s_index is an index into LIS, otherwise s_index
is an index into the octree corresponding to S. */
void
process_s(
  int D, int nx, int ny, int nz, int b, From where, int s_idx,
  const std::vector<Node>& octree, const VecInt8& leaves,
  IN_OUT VecInt* lis, IN_OUT VecInt* lip, OUT VecInt* lsp, OUT BitStream<>* stream)
{
  indent += 2;
#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Process S " << "\n";
#endif
  int sx, sy, sz, smx, smy, smz;
  int sb;
  int l_idx = -1; // index to leaves
  if (where == From::Octree) {
    const Node& s = octree[s_idx];
    i2xyz(s.xyz, nx, ny, nz, &sx, &sy, &sz);
    i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
    sb = s.b;
  }
  else if (where == From::Lis) {
    const Node& s = octree[(*lis)[s_idx]];
    i2xyz(s.xyz, nx, ny, nz, &sx, &sy, &sz);
    i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
    sb = s.b;
  }
  else { // where == Where::LIP
    l_idx = (*lip)[s_idx];
    i2xyz(l_idx, nx, ny, nz, &sx, &sy, &sz);
    smx = smy = smz = 1;
    sb = leaves[l_idx];
  }

  bool single_point = smx*smy*smz==1;
  stream->write_bit(sb >= b);
#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "  Write bit " << (sb >= b) << "\n";
#endif
  if (sb >= b) { // significant
    if (single_point) { // single point
      lsp->emplace_back(xyz2i(nx, ny, nz, sx, sy, sz));
      if (where == From::Lip) {
        (*lip)[s_idx] = -1;
      }
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Add to LSP " << sx << " " << sy << " " << sz << "\n";
      std::cout << std::string(indent, ' ') << "  Remove from LIP " << sx << " " << sy << " " << sz << "\n";
#endif
    }
    else { // not single point
      if (where == From::Octree) {
        code_s(D, nx, ny, nz, b, s_idx, octree, leaves, lis, lip, lsp, stream);
      }
      else if (where == From::Lis) {
        code_s(D, nx, ny, nz, b, (*lis)[s_idx], octree, leaves, lis, lip, lsp, stream);
        (*lis)[s_idx] = -1;
#ifdef SPECK_DEBUG
        std::cout << std::string(indent, ' ') << "  Remove from LIS " << sx << " " << sy << " " << sz << "\n";
#endif
      }
    }
  }
  else if (where == From::Octree) {
    if (single_point) {
      lip->emplace_back(xyz2i(nx, ny, nz, sx, sy, sz));
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Add to LIP " << sx << " " << sy << " " << sz << "\n";
#endif
    }
    else {
      lis->emplace_back(s_idx);
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Add to LIS " << sx << " " << sy << " " << sz << "\n";
#endif
    }
  }
  indent -= 2;
}

void
process_i(
  int D, int nx, int ny, int nz, int b, IN_OUT int* sb_idx,
  const std::vector<Node>& octree, const VecInt& octree_sb, const VecInt8& leaves,
  IN_OUT VecInt* lis, IN_OUT VecInt* lip, OUT VecInt* lsp, OUT BitStream<>* stream);

void
code_i(
  int D, int nx, int ny, int nz, int b, IN_OUT int* sb_idx,
  const std::vector<Node>& octree, const VecInt& octree_sb, const VecInt8& leaves,
  IN_OUT VecInt* lis, IN_OUT VecInt* lip, OUT VecInt* lsp, OUT BitStream<>* stream)
{
  indent += 2;
#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Code I " << "\n";
#endif
  REQUIRE(*sb_idx >= (1<<D)-2);
  for (int i = 0; i < (1<<D)-1; ++i) { // code the first seven subbands of I
    process_s(D, nx, ny, nz, b, From::Octree, octree_sb[*sb_idx-i], octree, leaves, lis, lip, lsp, stream);
  }
  *sb_idx -= (1<<D)-1;
  if (*sb_idx >= 0) {
    process_i(D, nx, ny, nz, b, sb_idx, octree, octree_sb, leaves, lis, lip, lsp, stream);
  }
  indent -= 2;
}

void
process_i(
  int D, int nx, int ny, int nz, int b, IN_OUT int* sb_idx,
  const std::vector<Node>& octree, const VecInt& octree_sb, const VecInt8& leaves,
  IN_OUT VecInt* lis, IN_OUT VecInt* lip, OUT VecInt* lsp, OUT BitStream<>* stream)
{
  indent += 2;

#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Process I " << "\n";
#endif
  REQUIRE(*sb_idx >= 0);
  bool i_sig = false;
  for (int i = *sb_idx; i >= 0; --i) { // for each subband
    if (octree[octree_sb[i]].b >= b) { // significant
      i_sig = true;
      break;
    }
  }
  stream->write_bit(i_sig);

#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "  Write bit " << i_sig << "\n";
#endif
  if (i_sig) { // I is significant, code it
    code_i(D, nx, ny, nz, b, sb_idx, octree, octree_sb, leaves, lis, lip, lsp, stream);
  }
  indent -= 2;
}

void
decode_s(
  int D, BitStream<>& stream, int nx, int ny, int nz, Set s, int b,
  IN_OUT std::vector<Set>* lis, IN_OUT VecInt* lip, OUT VecInt* lsp)
{
  indent += 2;

#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Decode S " << std::endl;
#endif
  const std::array<uint8_t, 8>& order = orders[D];
  int sx, sy, sz; i2xyz(s.xyz, nx, ny, nz, &sx, &sy, &sz);
  int smx, smy, smz; i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
  int px = (smx+1)>>1, py = (smy+1)>>1, pz = (smz+1)>>1;
  for (int i = 0; i < (1<<D); ++i) {
    uint8_t x = order[i]&1u, y = (order[i]>>1)&1u, z = (order[i]>>2)&1u;
    int cx = sx+x*px, cy = sy+y*py, cz = sz+z*pz;
    int cmx = (x==0)?px:smx-px, cmy = (y==0)?py:smy-py, cmz = (z==0)?pz:smz-pz;
    Set c = { xyz2i(nx, ny, nz, cx, cy, cz), xyz2i(nx, ny, nz, cmx, cmy, cmz) };
    if (cmx*cmy*cmz != 0) { // child exists
      uint sig = stream.read_bit();

#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Read bit " << sig << std::endl;
#endif
      bool single_point = cmx*cmy*cmz==1;
      if (sig > 0) { // significant
        if (single_point) { // single point
          lsp->emplace_back(c.xyz);
#ifdef SPECK_DEBUG
          std::cout << std::string(indent, ' ') << "  Add to LSP " << c.x << " " << c.y << " " << c.z << std::endl;
#endif
        }
        else { // not single point
          decode_s(D, stream, nx, ny, nz, c, b, lis, lip, lsp);
        }
      }
      else { // insignificant
        if (single_point) {
          lip->emplace_back(c.xyz);
#ifdef SPECK_DEBUG
          std::cout << std::string(indent, ' ') << "  Add to LIP " << c.x << " " << c.y << " " << c.z << std::endl;
#endif
        }
        else {
          lis->emplace_back(c);
#ifdef SPECK_DEBUG
          std::cout << std::string(indent, ' ') << "  Add to LIS " << c.x <<  " " << c.y << " " << c.z << std::endl;
#endif
        }
      }
    }
  }
  indent -= 2;
}

void
unprocess_s(
  int D, BitStream<>& stream, int nx, int ny, int nz,
  Set s, int b, From where, int s_idx,
  IN_OUT std::vector<Set>* lis, IN_OUT std::vector<int>* lip, std::vector<int>* lsp)
{
  indent += 2;

#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Unprocess S = " << std::endl;
#endif
  uint sig = stream.read_bit();
#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "  Read bit " << sig << std::endl;
#endif
  int sx, sy, sz; i2xyz(s.xyz, nx, ny, nz, &sx, &sy, &sz);
  int smx, smy, smz; i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
  bool single_point = smx*smy*smz==1;
  if (sig > 0) { // significant
    if (single_point) { // single point
      lsp->emplace_back(xyz2i(nx, ny, nz, sx, sy, sz));
      if (where == From::Lip) {
        (*lip)[s_idx] = -1;
      }
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Add to LSP " << s.x << " " << s.y << " "<< s.z << std::endl;
      std::cout << std::string(indent, ' ') << "  Remove from LIP " << s.x << " " << s.y << " " << s.z << std::endl;
#endif
    }
    else { // not single point
      decode_s(D, stream, nx, ny, nz, s, b, lis, lip, lsp);
    }
    if (where == From::Lis) { // S is in LIS, mark it for removal
      (*lis)[s_idx].xyz = -1;
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Remove from LIS " << s.x << " " << s.y << " " << s.z << std::endl;
#endif
    }
  }
  else if (where == From::Octree) {
    if (single_point) { // S is insignificant and not in LIS, add it to LIS
      lip->emplace_back(s.xyz);
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Add to LIP " << s.x << " " << s.y << " " << s.z << std::endl;
#endif
    }
    else {
      lis->emplace_back(s);
#ifdef SPECK_DEBUG
      std::cout << std::string(indent, ' ') << "  Add to LIS " << s.x << " " << s.y << " " << s.z << std::endl;
#endif
    }
  }
  indent -= 2;
}

void
unprocess_i(
  int D, BitStream<>& stream, int nx, int ny, int nz, int b,
  const std::vector<Set>& I, IN_OUT int* sb_index,
  IN_OUT std::vector<Set>* lis, IN_OUT std::vector<int>* lip, OUT std::vector<int>* lsp);

void
decode_i(
  int D, BitStream<>& stream, int nx, int ny, int nz, int b,
  const std::vector<Set>& I, IN_OUT int* sb_index,
  IN_OUT std::vector<Set>* lis, IN_OUT std::vector<int>* lip, OUT std::vector<int>* lsp)
{
  indent += 2;

#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Decode I "  << std::endl;
#endif
  REQUIRE(*sb_index >= (1<<D)-2);
  for (int i = 0; i < (1<<D)-1; ++i) { // decode the first seven subbands of I
    unprocess_s(D, stream, nx, ny, nz, I[(*sb_index)-i], b, From::Octree, -1, lis, lip, lsp);
  }
  *sb_index -= (1<<D)-1;
  if (*sb_index >= 0) {
    unprocess_i(D, stream, nx, ny, nz, b, I, sb_index, lis, lip, lsp);
  }
  indent -= 2;
}

void
unprocess_i(
  int D, BitStream<>& stream, int nx, int ny, int nz, int b,
  const std::vector<Set>& I, IN_OUT int* sb_index,
  IN_OUT std::vector<Set>* lis, IN_OUT std::vector<int>* lip, OUT std::vector<int>* lsp)
{
  indent += 2;
#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "Unprocess I " << std::endl;
#endif
  REQUIRE(*sb_index >= 0);
  uint sig = stream.read_bit();

#ifdef SPECK_DEBUG
  std::cout << std::string(indent, ' ') << "  Read bit = " << sig << std::endl;
#endif
  if (sig > 0) { // I is significant, decode it
    decode_i(D, stream, nx, ny, nz, b, I, sb_index, lis, lip, lsp);
  }
  indent -= 2;
}

template <typename T, typename = enable_if_unsigned<T>>
void
speck_encode(
  int D, const T* f, int nx, int ny, int nz, int nlevels, int prec, int bits,
  OUT BitStream<>* stream)
{
  REQUIRE(nlevels >= 1);
  REQUIRE(nx%(1<<nlevels) == 0);
  REQUIRE((D<2 && ny==1 && nz==1) || (ny%(1<<nlevels)==0));
  REQUIRE((D<3 && nz==1) || (nz%(1<<nlevels)==0));

  std::vector<Set> I;
  build_subbands(D, nx, ny, nz, nlevels, &I);
  std::vector<Node> octree; octree.reserve(1024);
  std::vector<int> octree_sb(I.size());
  std::vector<int8_t> leaves(nx*ny*nz);
  int node_idx = -1;
  for (size_t i = 0; i < I.size(); ++i) {
    Set& s = I[i];
    octree.emplace_back(Node{ s.xyz, s.mxyz, {}, -1 });
    ++node_idx;
    octree_sb[i] = node_idx;
    build_octree<T>(D, f, nx, ny, nz, &node_idx, &octree, &leaves);
  }

  /* compute and output the first threshold */
  int8_t B = -1; // the first threshold (first significant bit plane of everything)
  for (const int i : octree_sb) {
    B = std::max(B, octree[i].b);
  }
  stream->write_bits(B, 8);

  /* initialize LIS and LSP */
  std::vector<int> lis; lis.reserve(1024);
  std::vector<int> lip; lip.reserve(1024);
  const Set& s = I.back();
  int smx, smy, smz; i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
  if (smx*smy*smz != 1) {
    lis.push_back(octree_sb.back()); // add the root S to LIS
  }
  else {
    lip.push_back(s.xyz);
  }
  std::vector<int> lsp; lsp.reserve(nx*ny*nz);
  int sb_index = CINT(I.size()-2); // keeping track of the first subband of I

  /* encode */
  for (int b = B; b>=bits-prec && b>=0; --b) { // encode one bit plane at a time
    int lsp_end = CINT(lsp.size());
    int lis_end = CINT(lis.size());
    int lip_end = CINT(lip.size());
#ifdef SPECK_DEBUG
    std::cout << std::string(indent, ' ') << "Process LIP" << "\n";
#endif
    for (int i = 0; i < lip_end; ++i) { // code each point in LIP
      process_s(D, nx, ny, nz, b, From::Lip, i, octree, leaves, &lis, &lip, &lsp, stream);
    }
#ifdef SPECK_DEBUG
    std::cout << std::string(indent, ' ') << "End LIP" << "\n";
    std::cout << std::string(indent, ' ') << "Process LIS" << "\n";
#endif
    for (int i = 0; i < lis_end; ++i) { // code each set in LIS
      process_s(D, nx, ny, nz, b, From::Lis, i, octree, leaves, &lis, &lip, &lsp, stream);
    }
#ifdef SPECK_DEBUG
    std::cout << std::string(indent, ' ') << "End LIS" << "\n";
#endif
    lip.erase(std::remove_if(lip.begin(), lip.end(),
      [](const auto elem) { return elem == -1; }), lip.end());
    lis.erase(std::remove_if(lis.begin(), lis.end(),
      [](const auto elem) { return elem == -1; }), lis.end());
    if (sb_index >= 0) { // I is non empty
      process_i(D, nx, ny, nz, b, &sb_index, octree, octree_sb, leaves, &lis, &lip, &lsp, stream);
    }
    /* we skip the sorting pass */
    /* refinement pass */
    for (int i = 0; i < lsp_end; ++i) {
      int p = lsp[i];
      stream->write_bit((f[p]>>b)&1u);
    }
  }
}

template <typename T, typename = enable_if_unsigned<T>>
void
speck_decode(
  int D, BitStream<>& stream, int nx, int ny, int nz, int nlevels, int prec,
  int bits, OUT T* f)
{
  indent = 0;
  REQUIRE(nlevels >= 1);
  REQUIRE(nx%(1<<nlevels) == 0);
  REQUIRE((D<2 && ny==1 && nz==1) || (ny%(1<<nlevels)==0));
  REQUIRE((D<3 && nz==1) || (nz%(1<<nlevels)==0));

  std::fill(f, f+nx*ny*nz, 0);

  /* read the first threshold */
  int B = CINT(stream.read_bits(8));
  /* initialize LIS and LSP */
  std::vector<Set> I;
  build_subbands(D, nx, ny, nz, nlevels, &I);
  std::vector<Set> lis; lis.reserve(1024);
  std::vector<int> lip; lip.reserve(1024);
  const Set& s = I.back();
  int smx, smy, smz; i2xyz(s.mxyz, nx, ny, nz, &smx, &smy, &smz);
  if (smx*smy*smz != 1) {
    lis.push_back(s); // add the root S to LIS
  }
  else {
    lip.push_back(s.xyz);
  }
  int sb_index = CINT(I.size()-2); // keeping track of the first subband of I
  std::vector<int> lsp; lsp.reserve(nx*ny*nz);

  /* decode */
  for (int b = B; b>=bits-prec && b>=0; --b) {
    int lsp_end = CINT(lsp.size());
    int lis_end = CINT(lis.size());
    int lip_end = CINT(lip.size());
#ifdef SPECK_DEBUG
    std::cout << std::string(indent, ' ') << "Process LIP" << "\n";
#endif
    for (int i = 0; i < lip_end; ++i) { // decode each set in LIS
      Set ss = { lip[i], xyz2i(nx, ny, nz, 1, 1, 1) };
      unprocess_s(D, stream, nx, ny, nz, ss, b, From::Lip, i, &lis, &lip, &lsp);
    }
#ifdef SPECK_DEBUG
    std::cout << std::string(indent, ' ') << "End LIP" << "\n";
    std::cout << std::string(indent, ' ') << "Process LIS" << "\n";
#endif
    for (int i = 0; i < lis_end; ++i) { // decode each set in LIS
      unprocess_s(D, stream, nx, ny, nz, lis[i], b, From::Lis, i, &lis, &lip, &lsp);
    }
    //std::cout << std::string(indent, ' ') << "End LIS" << "\n";
    lip.erase(std::remove_if(lip.begin(), lip.end(),
      [](const auto elem) { return elem == -1; }), lip.end());
    lis.erase(std::remove_if(lis.begin(), lis.end(),
      [](const auto& elem) { return elem.xyz == -1; }), lis.end());
    if (sb_index >= 0) { // I is non empty
      unprocess_i(D, stream, nx, ny, nz, b, I, &sb_index, &lis, &lip, &lsp);
    }
    /* refinement pass */
    for (int i = 0; i < lsp_end; ++i) {
      int p = lsp[i];
      T bit = SCAST<T>(stream.read_bit());
      f[p] |= (bit<<b);
    }
    int lsp_size = CINT(lsp.size());
    for (int i = lsp_end; i < lsp_size; ++i) {
      int p = lsp[i];
      f[p] |= (T(1)<<(b));
    }
  }
}
